//
//  SceneDelegate.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property(class, nonatomic, readonly) SceneDelegate *sharedSceneDelegate;

@property (strong, nonatomic) UIWindow * window;


@end

