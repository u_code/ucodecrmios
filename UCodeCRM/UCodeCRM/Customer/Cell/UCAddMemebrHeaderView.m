//
//  UCAddMemebrHeaderView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddMemebrHeaderView.h"
#import "UCBaseView.h"
#import "UCAddMemebrHeaderViewCell.h"

@interface UCAddMemebrHeaderView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation UCAddMemebrHeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(44, 44);
        layout.sectionInset = UIEdgeInsetsMake(6, 16, 0, 16);
//        layout.footerReferenceSize = CGSizeMake(SCREEN_WIDTH, 189 + SCREEN_HEIGHT);
        layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        collectionView.showsHorizontalScrollIndicator = NO;
        [collectionView registerClass:[UCAddMemebrHeaderViewCell class] forCellWithReuseIdentifier:@"UCAddMemebrHeaderViewCell"];
        [self.contentView addSubview:collectionView];
        collectionView.backgroundColor = [UIColor whiteColor];
        
        //            [collectionView registerClass:[UCTrackingCollectionFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"UCTrackingCollectionFooterView"];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        
        // Do any additional setup after loading the view.
    }
    
    return self;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 10;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UCAddMemebrHeaderViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UCAddMemebrHeaderViewCell" forIndexPath:indexPath];
    return cell;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//        - (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
//
//            UCTrackingCollectionFooterView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"UCTrackingCollectionFooterView" forIndexPath:indexPath];
//            return footerView;
//        }

@end
