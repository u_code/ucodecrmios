//
//  UCAddMemberCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddMemberCell.h"

@interface UCAddMemberCell ()

@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, strong) UILabel *headerNameLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *numberLabel;

@end

@implementation UCAddMemberCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIButton *selectedButton = [UIButton buttonWithSuperView:self.contentView target:self action:@selector(selectedButtonAction:) imageName:@"stage_selected"];
        [selectedButton setImage:[UIImage imageNamed:@"stage_selected"] forState:UIControlStateSelected];
        [selectedButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.equalTo(self.contentView).offset(17);
            make.centerY.equalTo(self.contentView);
        }];
        self.selectedButton = selectedButton;
        
        UIImageView *headerImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@""];
        [headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(selectedButton.mas_right).offset(11);
            make.size.mas_equalTo(CGSizeMake(44, 44));
            make.centerY.equalTo(self.contentView);
        }];
        headerImage.layer.cornerRadius = 22;
        headerImage.clipsToBounds = YES;
        headerImage.backgroundColor = [UIColor colorWithHexString:@"#9EA4B1"];
        
        UILabel *headerNameLabel = [UILabel labelWithSuperView:headerImage textColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] text:@"永军"];
        [headerNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(headerImage);
        }];
        self.headerNameLabel = headerNameLabel;
        
        UILabel *nameLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"永军"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(19);
            make.left.equalTo(headerImage.mas_right).offset(16);
        }];
        self.nameLabel = nameLabel;
        
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(46);
            make.right.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(0.5);
        }];
        
    }
    return self;
}

-(void)selectedButtonAction:(UIButton *)button{
    
}

@end
