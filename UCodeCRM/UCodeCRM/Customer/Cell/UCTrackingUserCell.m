//
//  UCTrackingUserCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCTrackingUserCell.h"
#import "UCBaseView.h"

@interface UCTrackingUserCell ()

@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel     *headerNameLabel;
@property (nonatomic, strong) UILabel     *nameLabel;

@end

@implementation UCTrackingUserCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *headerImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@""];
        [headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(52, 52));
            make.top.equalTo(self.contentView);
            make.centerX.equalTo(self.contentView);
        }];
        headerImage.layer.cornerRadius = 26;
        headerImage.clipsToBounds = YES;
        headerImage.backgroundColor = [UIColor colorWithHexString:@"#9EA4B1"];
        self.headerImage = headerImage;
        
        UILabel *headerNameLabel = [UILabel labelWithSuperView:headerImage textColor:[UIColor whiteColor] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 18.9] text:@"乃琛"];
        [headerNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(headerImage);
        }];
        self.headerNameLabel = headerNameLabel;
        
        UILabel *nameLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:12] text:@"乃琛"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headerImage.mas_bottom).offset(8);
            make.centerX.equalTo(self.contentView);
        }];
        self.nameLabel = nameLabel;
        
    }
    return self;
}
@end
