//
//  UCSearchUserCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCSearchUserCell.h"

@interface UCSearchUserCell ()

@property (nonatomic, strong) UILabel *headerNameLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *numberLabel;

@end

@implementation UCSearchUserCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *headerImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@""];
        [headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.size.mas_equalTo(CGSizeMake(44, 44));
            make.centerY.equalTo(self.contentView);
        }];
        headerImage.layer.cornerRadius = 22;
        headerImage.clipsToBounds = YES;
        headerImage.backgroundColor = [UIColor colorWithHexString:@"#9EA4B1"];
        
        UILabel *headerNameLabel = [UILabel labelWithSuperView:headerImage textColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:16] text:@"永军"];
        [headerNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(headerImage);
        }];
        self.headerNameLabel = headerNameLabel;
        
        UILabel *nameLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"永军"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(19);
            make.left.equalTo(headerImage.mas_right).offset(16);
        }];
        self.nameLabel = nameLabel;
        
        UILabel *numberLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#AAAAAA"] font:[UIFont systemFontOfSize:12] text:@"2132434535345"];
        [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLabel);
            make.top.equalTo(nameLabel.mas_bottom).offset(12);
        }];
        self.numberLabel = numberLabel;
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.right.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(0.5);
        }];
        
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
