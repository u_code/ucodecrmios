//
//  UCOpportunityStageCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/2/17.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCOpportunityStageCell.h"

@interface UCOpportunityStageCell ()

@property (nonatomic, strong) UIButton  *selectedButton;
@property (nonatomic, strong) UILabel   *stageLabel;
@property (nonatomic, strong) UILabel   *percentLabel;

@end

@implementation UCOpportunityStageCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIButton *selectedButton = [UIButton buttonWithSuperView:self.contentView target:self action:@selector(selectedButtonAction:) imageName:@"stage_selected"];
        [selectedButton setImage:[UIImage imageNamed:@"stage_selected"] forState:UIControlStateSelected];
        [selectedButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.equalTo(self.contentView).offset(16);
            make.centerY.equalTo(self.contentView);
        }];
        self.selectedButton = selectedButton;
        
        UILabel *stageLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"阶段1"];
               [stageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(selectedButton.mas_right).offset(10);
                   make.centerY.equalTo(self.contentView);
               }];
               self.stageLabel = stageLabel;
        
   UILabel *percentLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#999999"] font:[UIFont systemFontOfSize:16] text:@"赢单率达到20%"];
        [percentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(stageLabel.mas_right).offset(10);
            make.centerY.equalTo(self.contentView);
        }];
        self.percentLabel = percentLabel;
        
        
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
               [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(46);
                   make.bottom.equalTo(self.contentView);
                   make.right.equalTo(self.contentView);
                   make.height.mas_equalTo(1);
               }];
        
    }
    return self;
}

-(void)selectedButtonAction:(UIButton *)button{
    
}


//   @{@"stage" : @"赢单" , @"percent" : @"赢单率100%" ,       @"isSelected" : @"0" },
-(void)configureWithViewModel:(NSDictionary *)ViewModel{
    self.stageLabel.text = ViewModel[@"stage"];
    self.percentLabel.text = ViewModel[@"percent"];
//    self.textField.text = ViewModel[@"value"];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
