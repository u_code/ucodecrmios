//
//  UCOpportunityInforCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCOpportunityInforCell.h"
#import "UCProgressView.h"

@interface UCOpportunityInforCell ()

@property (nonatomic, strong) UILabel *presentLabel;
@property (nonatomic, strong) UILabel *titleLabel;
//@property (nonatomic, strong) UCProgressView *progressView;


@end

@implementation UCOpportunityInforCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *leftView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor whiteColor]];
        [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(17);
            make.size.mas_offset(CGSizeMake(42, 42));
            make.centerY.equalTo(self.contentView);
        }];
        leftView.layer.borderWidth = 3;
        leftView.layer.cornerRadius = 21;
        leftView.layer.borderColor = [UIColor colorWithHexString:@"#EFEFEF"].CGColor;
        leftView.clipsToBounds = YES;
        
        UCProgressView *progressView = [UCProgressView viewWithSuperView:self.contentView];
        [progressView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.equalTo(leftView);
//            make.size.mas_equalTo(CGSizeMake(36, 36));
            make.left.equalTo(self.contentView).offset(18.5);
            make.size.mas_offset(CGSizeMake(39, 39));
            make.centerY.equalTo(self.contentView);
        }];
        self.progressView = progressView;
        progressView.progress = 0.3;
        
        UILabel *presentLabel = [UILabel labelWithSuperView:leftView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"3/4"];
        [presentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(leftView);
        }];
        self.presentLabel = presentLabel;
        
        UILabel *titleLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"3/4"];
              [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                  make.left.equalTo(leftView.mas_right).offset(16);
                  make.centerY.equalTo(self.contentView);
              }];
              self.titleLabel = titleLabel;
        
        UIImageView *arrowImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@"arrow_right"];
                       [arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
                           make.right.equalTo(self.contentView).offset(-17);
                           make.size.mas_equalTo(CGSizeMake(14, 14));
                           make.centerY.equalTo(self.contentView);
                       }];
        //               arrowImage.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
                
                
                UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
                       [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                           make.left.equalTo(self.contentView).offset(16);
                           make.bottom.equalTo(self.contentView);
                           make.right.equalTo(self.contentView);
                           make.height.mas_equalTo(1);
                       }];
        
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
