//
//  UCCustomerCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerCell.h"

@interface UCCustomerCell ()

@property (nonatomic, strong) UIView *backGroundView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *userInforLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *remarkLabel;

@end

@implementation UCCustomerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *iconImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@""];
        [iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.contentView).offset(16);
            make.size.mas_equalTo(CGSizeMake(48, 48));
        }];
        iconImage.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
        
        UILabel *nameLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"刘洪林"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(iconImage.mas_right).offset(16);
            make.top.equalTo(self.contentView).offset(15);
        }];
        self.nameLabel = nameLabel;
        
        UILabel *timeLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#AAAAAA"] font:[UIFont systemFontOfSize:12] text:@"昨天13:12"];
               [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.right.equalTo(self.mas_right).offset(-16);
                   make.centerY.equalTo(nameLabel);
               }];
               self.timeLabel = timeLabel;
        
        UILabel *userInforLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#AAAAAA"] font:[UIFont systemFontOfSize:12] text:@"贵阳 · 夏庆超 · 23天 · 25次更新 · 奥迪A6L"];
               [userInforLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(iconImage.mas_right).offset(16);
                   make.top.equalTo(nameLabel.mas_bottom).offset(6);
                    make.right.equalTo(self.contentView).offset(-17);
               }];
               self.userInforLabel = userInforLabel;
        
        UILabel *remarkLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"客户询价奥迪A6L，希望置换购车，明天可…"];
               [remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(iconImage.mas_right).offset(16);
                    make.bottom.equalTo(self.mas_bottom).offset(-16);
                   make.right.equalTo(self.contentView).offset(-17);
               }];
               self.remarkLabel = remarkLabel;
        
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(80);
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
        
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}

@end
