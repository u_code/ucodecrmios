//
//  UCSearchUserTableHeaderView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/2/19.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UCSearchUserTableHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
