//
//  UCOpportunityInforCell.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseCell.h"
#import "UCProgressView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCOpportunityInforCell : UCBaseCell

@property (nonatomic, strong) UCProgressView *progressView;

@end

NS_ASSUME_NONNULL_END
