//
//  UCCustomerUserInforCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerUserInforCell.h"

@interface UCCustomerUserInforCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel   *contentLable;


@end

@implementation UCCustomerUserInforCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *titleLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"刘洪林"];
               [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(16);
                   make.centerY.equalTo(self.contentView);
               }];
               self.titleLabel = titleLabel;
        
        UILabel *contentLable = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"刘洪林"];
               [contentLable mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(90);
                   make.centerY.equalTo(self.contentView);
                   make.right.equalTo(self.contentView).offset(-16);
               }];
        contentLable.textAlignment = NSTextAlignmentRight;
        self.contentLable = contentLable;
        
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
               [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(16);
                   make.bottom.equalTo(self.contentView);
                   make.right.equalTo(self.contentView);
                   make.height.mas_equalTo(1);
               }];
        
    }
    return self;
}

//   @{@"title" : @"客户姓名" , @"placeholder" : @"请填写客户姓名(必填)" ,       @"value" : @"" },
-(void)configureWithViewModel:(NSDictionary *)ViewModel{
    self.titleLabel.text = ViewModel[@"title"];
    self.contentLable.text = ViewModel[@"value"];
}

@end
