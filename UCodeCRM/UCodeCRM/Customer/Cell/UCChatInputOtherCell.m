//
//  UCChatInputOtherCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/10.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCChatInputOtherCell.h"
#import "UCBaseCell.h"

@interface UCChatInputOtherCell ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel     *titleLabel;

@end

@implementation UCChatInputOtherCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *imageView = [UIImageView imageViewWithSuperView:self.contentView imageName:@""];
        self.imageView = imageView;
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.centerX.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(64, 64));
        }];
        imageView.backgroundColor = [UIColor colorWithHexString:@"#F6F6F6"];
        
        UILabel *titleLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:12] text:@"照片"];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(imageView.mas_bottom).offset(4);
        }];
        
        
        
        
        
        
    }
    return self;
}

- (void)configureWithViewModel:(UCChatInputOtherModel *)ViewModel{
    self.titleLabel.text = ViewModel.title;
}
@end
