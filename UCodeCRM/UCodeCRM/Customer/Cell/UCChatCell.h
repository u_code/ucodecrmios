//
//  UCChatCell.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/11.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCChatCell : UCBaseCell

@property (nonatomic, strong) UIView        *otherMessageView;
@property (nonatomic, strong) UILabel       *otherNameLabel;

@property (nonatomic, strong) UIImageView   *otherTextMessageView;
@property (nonatomic, strong) UILabel       *otherTextMessageLable;

@property (nonatomic, strong) UIImageView   *otherImageMessage;

@property (nonatomic, strong) UIView        *myMessageView;
@property (nonatomic, strong) UILabel       *myNameLabel;

@property (nonatomic, strong) UIImageView   *myTextMessageView;
@property (nonatomic, strong) UILabel       *myTextMessageLable;

@property (nonatomic, strong) UIImageView   *myImageMessage;

@property (nonatomic, assign) NSInteger     messageType;

@end

NS_ASSUME_NONNULL_END
