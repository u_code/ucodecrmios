//
//  UCChatCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/11.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCChatCell.h"

@interface UCChatCell ()

//@property (nonatomic, strong) UIView        *otherMessageView;
//@property (nonatomic, strong) UILabel       *otherNameLabel;
//
//@property (nonatomic, strong) UIImageView   *otherTextMessageView;
//@property (nonatomic, strong) UILabel       *otherTextMessageLable;
//
//@property (nonatomic, strong) UIImageView   *otherImageMessage;
//
//@property (nonatomic, strong) UIView        *myMessageView;
//@property (nonatomic, strong) UILabel       *myNameLabel;
//
//@property (nonatomic, strong) UIImageView   *myTextMessageView;
//@property (nonatomic, strong) UILabel       *myTextMessageLable;
//
//@property (nonatomic, strong) UIImageView   *myImageMessage;





@end

@implementation UCChatCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
// 其它人的消息
        UIView *otherMessageView = [UIView viewWithSuperView:self.contentView];
        self.otherMessageView = otherMessageView;
        [otherMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).insets(UIEdgeInsetsMake(20, 0, 0, 0));
        }];
        
        UILabel *otherNameLabel = [UILabel labelWithSuperView:otherMessageView textColor:[UIColor whiteColor] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 16] text:@"丽娇"];
        self.otherNameLabel = otherNameLabel;
        [otherNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(otherMessageView).offset(16);
            make.top.equalTo(otherMessageView);
            make.size.mas_equalTo(CGSizeMake(44, 44));
        }];
        self.otherNameLabel = otherNameLabel;
        otherNameLabel.backgroundColor = [UIColor colorWithHexString:@"#9EA4B1"];
        otherNameLabel.layer.cornerRadius = 22;
        otherNameLabel.textAlignment = NSTextAlignmentCenter;
        otherNameLabel.clipsToBounds = YES;
        
// 他人的文字
        UIImageView *otherTextMessageView = [UIImageView imageViewWithSuperView:otherMessageView imageName:@""];
        [otherTextMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(otherMessageView);
            make.left.equalTo(otherNameLabel.mas_right).offset(10);
            make.right.lessThanOrEqualTo(otherMessageView.mas_right).offset(-16);
            make.bottom.equalTo(otherMessageView);
        }];
        self.otherTextMessageView = otherTextMessageView;
        otherTextMessageView.image = [[UIImage imageNamed:@"message_of_other"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 26, 18, 16) resizingMode:UIImageResizingModeStretch];
        
        UILabel   *otherTextMessageLable = [UILabel labelWithSuperView:otherTextMessageView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"已经过了信审，下一阶段由勇哥准备车辆 @勇哥"];
        [otherTextMessageLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(otherTextMessageView).insets(UIEdgeInsetsMake(18, 26, 18, 16));
        }];
        otherTextMessageLable.numberOfLines = 0;
        self.otherTextMessageLable = otherTextMessageLable;
        
//  他人的图片消息
        
        UIImageView *otherImageMessage = [UIImageView imageViewWithSuperView:otherMessageView imageName:@""];
        [otherImageMessage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(otherMessageView);
//             make.top.equalTo(otherMessageView);
            make.left.equalTo(otherNameLabel.mas_right).offset(10);
            make.right.lessThanOrEqualTo(otherMessageView).offset(-16);
        }];
        otherImageMessage.contentMode = UIViewContentModeScaleAspectFit;
        self.otherImageMessage = otherImageMessage;
        otherImageMessage.hidden = YES;
        otherImageMessage.backgroundColor = [UIColor redColor];
        otherImageMessage.layer.cornerRadius = 4;
        
        
        
//        @property (nonatomic, strong) UIView        *myMessageView;
//        @property (nonatomic, strong) UILabel       *myNameLabel;
//
//        @property (nonatomic, strong) UIImageView   *myTextMessageView;
//        @property (nonatomic, strong) UILabel       *myTextMessageLable;
//
//        @property (nonatomic, strong) UIImageView   *myImageMessage;
        
        // 自己的消息
                UIView *myMessageView = [UIView viewWithSuperView:self.contentView];
                self.myMessageView = myMessageView;
                [myMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(self.contentView).insets(UIEdgeInsetsMake(20, 0, 0, 0));
                }];
                
                UILabel *myNameLabel = [UILabel labelWithSuperView:myMessageView textColor:[UIColor whiteColor] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 16] text:@"丽娇"];
                self.myNameLabel = myNameLabel;
                [myNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.equalTo(myMessageView).offset(-16);
                    make.top.equalTo(myMessageView);
                    make.size.mas_equalTo(CGSizeMake(44, 44));
                }];
                self.myNameLabel = myNameLabel;
                myNameLabel.backgroundColor = [UIColor colorWithHexString:@"#9EA4B1"];
                myNameLabel.layer.cornerRadius = 22;
                myNameLabel.textAlignment = NSTextAlignmentCenter;
                myNameLabel.clipsToBounds = YES;
                
        // 自己的文字
                UIImageView *myTextMessageView = [UIImageView imageViewWithSuperView:myMessageView imageName:@""];
                [myTextMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(myMessageView);
                    make.right.equalTo(myNameLabel.mas_left).offset(-10);
                    make.left.greaterThanOrEqualTo(myMessageView).offset(16);
                    make.bottom.equalTo(myMessageView);
                }];
                self.myTextMessageView = myTextMessageView;
                myTextMessageView.image = [[UIImage imageNamed:@"message_of_self"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 16, 18, 26) resizingMode:UIImageResizingModeStretch];
                
                UILabel   *myTextMessageLable = [UILabel labelWithSuperView:myTextMessageView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"已经过了信审，下一阶段由勇哥准备车辆 @勇哥"];
                [myTextMessageLable mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(myTextMessageView).insets(UIEdgeInsetsMake(18, 16, 18, 26));
                }];
                myTextMessageLable.numberOfLines = 0;
//                myTextMessageLable.textAlignment = NSTextAlignmentRight;
                self.myTextMessageLable = myTextMessageLable;
                
        //  自己的图片消息
                
                UIImageView *myImageMessage = [UIImageView imageViewWithSuperView:myMessageView imageName:@""];
                [myImageMessage mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(myMessageView);
                    make.right.equalTo(myNameLabel.mas_left).offset(-10);
                    make.left.greaterThanOrEqualTo(myMessageView).offset(16);
                    make.bottom.equalTo(myMessageView);
                }];
                myImageMessage.contentMode = UIViewContentModeScaleAspectFit;
                self.myImageMessage = myImageMessage;
        myImageMessage.backgroundColor = [UIColor blueColor];
        myImageMessage.layer.cornerRadius = 4;
        
        
        
        
        
    }
    return self;
}


-(void)setMessageType:(NSInteger)messageType{
    
    NSString *string = @"2020年3月5日上午，黎川县公安局接到《问政抚州》舆情“黎川县冠建小区东纬七路12号有人聚众赌博”。对此，黎川县公安局高度重视，上午10点45分，黎川县公安局特巡警大队采取隐蔽方法到达现场了解情况，未发现有人在杂货店内打麻将。下午2点40分，特巡警大队再次来到冠建小区东纬七路，先后在冠建小区东纬七路11号、12号发现有人正在以打麻将的方式进行赌博。特巡警大队巡警立即通知日峰派出所民警前往处理。日峰派出所民警到达现场后，将在场的8人依法口头传唤至日峰派出所接受询问";
    NSInteger length = arc4random()%200;
    NSInteger imageHeight = arc4random()%200 + 44;
    
    if (messageType < 2) {
        self.myMessageView.hidden = YES;
        self.otherMessageView.hidden = NO;
        self.myTextMessageLable.text = @"";
        
    }else{
        self.myMessageView.hidden = NO;
        self.otherMessageView.hidden = YES;
         self.otherTextMessageLable.text = @"";
    }
//    [_myImageMessage mas_updateConstraints:^(MASConstraintMaker *make) {
//               make.height.mas_equalTo(0);
//        }];
//    [_otherImageMessage mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(0);
//    }];
    
    if (messageType == 0) { //他人文字消息

        self.otherTextMessageLable.text = [string substringToIndex:length];
        self.otherTextMessageView.hidden = NO;
        self.otherImageMessage.hidden = YES;
        
    }else if (messageType == 1){ //他人图片
        
        self.otherTextMessageLable.text = @"";
        self.otherTextMessageView.hidden = YES;
        self.otherImageMessage.hidden = NO;
//        [_otherImageMessage mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(imageHeight);
//        }];
        
    }else if (messageType == 2){ //自己文字
        self.myTextMessageLable.text = [string substringToIndex:length];
        self.myTextMessageView.hidden = NO;
        self.myImageMessage.hidden = YES;
        
    }else if (messageType == 3){ //自己图片
        self.myTextMessageLable.text = @"";
        self.myTextMessageView.hidden = YES;
        self.myImageMessage.hidden = NO;
//        [_myImageMessage mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(imageHeight);
//        }];
        
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
