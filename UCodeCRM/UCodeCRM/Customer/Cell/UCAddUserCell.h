//
//  UCAddUserCell.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCAddUserCell : UCBaseCell

@property (nonatomic, copy) BOOL(^textFieldShouldBeginEditing)(UITextField *textField);

@end

NS_ASSUME_NONNULL_END
