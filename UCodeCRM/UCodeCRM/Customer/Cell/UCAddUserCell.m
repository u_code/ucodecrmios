//
//  UCAddUserCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddUserCell.h"

@interface UCAddUserCell ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField   *textField;


@end

@implementation UCAddUserCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *titleLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"刘洪林"];
               [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(16);
                   make.centerY.equalTo(self.contentView);
               }];
               self.titleLabel = titleLabel;
        
        UITextField *textField = [UITextField viewWithSuperView:self.contentView];
               [textField mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(90);
                   make.centerY.equalTo(self.contentView);
                   make.right.equalTo(self.contentView).offset(-48);
               }];
        textField.textAlignment = NSTextAlignmentRight;
        textField.delegate = self;
        self.textField = textField;
        
        UIImageView *arrowImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@"arrow_right"];
               [arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.right.equalTo(self.contentView).offset(-17);
                   make.size.mas_equalTo(CGSizeMake(14, 14));
                   make.centerY.equalTo(self.contentView);
               }];
//               arrowImage.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
        
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
               [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.equalTo(self.contentView).offset(16);
                   make.bottom.equalTo(self.contentView);
                   make.right.equalTo(self.contentView);
                   make.height.mas_equalTo(1);
               }];
        
    }
    return self;
}

//   @{@"title" : @"客户姓名" , @"placeholder" : @"请填写客户姓名(必填)" ,       @"value" : @"" },
-(void)configureWithViewModel:(NSDictionary *)ViewModel{
    self.titleLabel.text = ViewModel[@"title"];
    self.textField.placeholder = ViewModel[@"placeholder"];
    self.textField.text = ViewModel[@"value"];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return self.textFieldShouldBeginEditing(textField);
}

@end
