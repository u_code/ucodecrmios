//
//  UCAddMemebrHeaderViewCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddMemebrHeaderViewCell.h"
#import "UCBaseCell.h"

@interface UCAddMemebrHeaderViewCell ()

@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel     *nameLabel;


@end

@implementation UCAddMemebrHeaderViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *headerImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@""];
        [headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(44, 44));
        }];
        headerImage.backgroundColor = [UIColor colorWithHexString:@"#9EA4B1"];
        headerImage.layer.cornerRadius = 22;
        headerImage.clipsToBounds = YES;
        self.headerImage = headerImage;
        
        UILabel *nameLabel = [UILabel labelWithSuperView:headerImage textColor:[UIColor whiteColor] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 16] text:@"庆超"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(headerImage);
        }];
        self.nameLabel = nameLabel;
    }
    return self;
}

@end
