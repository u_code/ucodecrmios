//
//  UCSearchUserTableHeaderView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/2/19.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCSearchUserTableHeaderView.h"
#import "UCBaseCell.h"

@implementation UCSearchUserTableHeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *titleLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@"#AAAAAA"] font:[UIFont systemFontOfSize:14] text:@"客户"];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.bottom.equalTo(self.contentView);
        }];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
