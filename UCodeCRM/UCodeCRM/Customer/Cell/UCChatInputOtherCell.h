//
//  UCChatInputOtherCell.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/10.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UCChatInputOtherModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface UCChatInputOtherCell : UICollectionViewCell

- (void)configureWithViewModel:(UCChatInputOtherModel *)ViewModel;
@end

NS_ASSUME_NONNULL_END
