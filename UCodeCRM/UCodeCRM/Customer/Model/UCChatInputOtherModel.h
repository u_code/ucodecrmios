//
//  UCChatInputOtherModel.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/10.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCChatInputOtherModel : UCBaseModel

@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
