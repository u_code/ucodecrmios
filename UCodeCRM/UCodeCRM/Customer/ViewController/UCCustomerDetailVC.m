//
//  UCCustomerDetailVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerDetailVC.h"
#import "UCCustomerOpportunityVC.h"
#import "UCCustomerUserInforVC.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"


@interface UCCustomerDetailVC ()<FSPageContentViewDelegate,FSSegmentTitleViewDelegate>

@property (nonatomic, strong) FSPageContentView *pageContentView;
@property (nonatomic, strong) FSSegmentTitleView *titleView;

@end

@implementation UCCustomerDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIView *titleView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
//    [titleView mas_ma]
    [self addBackItem];
    FSSegmentTitleView *titleView = [[FSSegmentTitleView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 150, 44) titles:@[@"客户信息",@"商机信息"] delegate:self indicatorType:3];
    self.titleView = titleView;
    titleView.titleSelectFont =  [UIFont fontWithName:@"PingFangSC-Semibold" size: 18];
    titleView.backgroundColor = [UIColor whiteColor];
    titleView.titleFont = [UIFont systemFontOfSize:16];
    titleView.titleSelectColor = [UIColor colorWithHexString:@"#1A1616"];
    titleView.titleNormalColor = [UIColor colorWithHexString:@"#666666"];
    self.navigationItem.titleView = titleView;
    
    UCCustomerUserInforVC *userInfor = [[UCCustomerUserInforVC alloc] init];
    UCCustomerOpportunityVC *customerOpportunityvc = [[UCCustomerOpportunityVC alloc ] init];
    
    
      self.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 90) childVCs:@[userInfor, customerOpportunityvc] parentVC:self delegate:self];
        self.pageContentView.contentViewCurrentIndex = 2;
    //    self.pageContentView.contentViewCanScroll = NO;//设置滑动属性
        [self.view addSubview:_pageContentView];
    
}

- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.pageContentView.contentViewCurrentIndex = endIndex;
    self.title = @[@"全部",@"服饰穿搭",@"生活百货",@"美食吃货",@"美容护理",@"母婴儿童",@"数码家电",@"其他"][endIndex];
}

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    self.titleView.selectIndex = endIndex;
    self.title = @[@"全部",@"服饰穿搭",@"生活百货",@"美食吃货",@"美容护理",@"母婴儿童",@"数码家电",@"其他"][endIndex];
}

@end
