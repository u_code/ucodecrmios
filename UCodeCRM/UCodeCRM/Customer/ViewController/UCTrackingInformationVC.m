//
//  UCTrackingInformationVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCTrackingInformationVC.h"
#import "UCTrackingCollectionFooterView.h"
#import "UCTrackingUserCell.h"
#import "UCAddMemberVC.h"

@interface UCTrackingInformationVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation UCTrackingInformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"跟踪信息(8)";
    [self addBackItem];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(52, 92);
    layout.sectionInset = UIEdgeInsetsMake(20, 20, 0, 20);
    layout.footerReferenceSize = CGSizeMake(SCREEN_WIDTH, 189 + SCREEN_HEIGHT);
    layout.minimumInteritemSpacing = 18;
    layout.minimumLineSpacing = 0;
    
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collectionView registerClass:[UCTrackingUserCell class] forCellWithReuseIdentifier:@"UCTrackingUserCell"];
    [self.view addSubview:collectionView];
    collectionView.backgroundColor = [UIColor whiteColor];

    [collectionView registerClass:[UCTrackingCollectionFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"UCTrackingCollectionFooterView"];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];

    
    // Do any additional setup after loading the view.
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
  
    if (indexPath.row == 8) {
        UCAddMemberVC *addMembervc = [[UCAddMemberVC alloc] init];
        [self.navigationController pushViewController:addMembervc animated:YES];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 
    return 10;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UCTrackingUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UCTrackingUserCell" forIndexPath:indexPath];
    return cell;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UCTrackingCollectionFooterView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"UCTrackingCollectionFooterView" forIndexPath:indexPath];
    return footerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
