//
//  UCCustomerOpportunityVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerOpportunityVC.h"
#import "UCOpportunityInforCell.h"
#import "UCCustomerLevelVC.h"

@interface UCCustomerOpportunityVC ()

<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;

@end

@implementation UCCustomerOpportunityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    [tablView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"UITableViewHeaderFooterView"];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    tablView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    
    UIButton *addOpportunityButton = [UIButton buttonWithSuperView:nil target:self action:@selector(addOpportunityButtonAction:) title:@"+ 添加商机" titleColor:[UIColor colorWithHexString:@"#23B3C1"]];
    addOpportunityButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, 56);
    tablView.tableFooterView = addOpportunityButton;
    addOpportunityButton.backgroundColor = [UIColor whiteColor];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        UCOpportunityInforCell *cell = [UCOpportunityInforCell cellWithTableView:tableView];
//        [cell configureWithViewModel:subArray[indexPath.row]];
    cell.progressView.progress = indexPath.row * 0.2;
          return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"UITableViewHeaderFooterView"];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 84;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UCCustomerLevelVC *leveVc = [[UCCustomerLevelVC alloc] init];
    [self.navigationController pushViewController:leveVc animated:YES];
}

-(void)addOpportunityButtonAction:(UIButton *)button{
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
