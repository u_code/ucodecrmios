//
//  UCCallVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCallVC.h"

@interface UCCallVC ()

@end

@implementation UCCallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.5];
    
    UIButton *canCelButton = [UIButton buttonWithSuperView:self.view target:self action:@selector(cancelButtonAction) title:@"取消" titleColor:[UIColor colorWithHexString:@"#1A1616"]];
    canCelButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:17];
    [canCelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(59);
        make.bottom.left.right.equalTo(self.view).insets(UIEdgeInsetsMake(0, 20, 44, 20));
    }];
    canCelButton.backgroundColor = [UIColor whiteColor];
    canCelButton.layer.cornerRadius = 10;
    canCelButton.clipsToBounds = YES;
    
    UIView *contentView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 10;
    contentView.clipsToBounds = YES;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(70);
        make.bottom.left.right.equalTo(self.view).insets(UIEdgeInsetsMake(0, 20, 112, 20));
    }];
    
    UIView *lastView ;
    for (int i = 0; i < 1; i++) {
        UIControl *control = [UIControl viewWithSuperView:contentView];
        control.tag = i;
        [control addTarget:self action:@selector(callPhoneAction:) forControlEvents:UIControlEventTouchUpInside];
        lastView = control;
        [control mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(70);
            make.left.right.equalTo(contentView);
            if (lastView) {
                make.bottom.equalTo(lastView.mas_top);
            }else{
                make.bottom.equalTo(contentView);
            }
        }];
        
        UILabel *nameLabel = [UILabel labelWithSuperView:control textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont fontWithName:@"PingFangSC-Medium" size:17] text:@"刘洪林"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(control);
            make.top.equalTo(control).offset(10);
        }];
        
        
        UILabel *phoneLabel = [UILabel labelWithSuperView:control textColor:[UIColor colorWithHexString:@"#AAAAAA"] font:[UIFont systemFontOfSize:12] text:@"17733325100"];
        [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(control);
            make.top.equalTo(nameLabel.mas_bottom).offset(5);
        }];
        
    }
    // Do any additional setup after loading the view.
}

-(void)callPhoneAction:(UIControl *)controller{
    [self cancelButtonAction];
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"17733325100"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)cancelButtonAction{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if ([touch.view isEqual:self.view]) {
        [self cancelButtonAction];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
