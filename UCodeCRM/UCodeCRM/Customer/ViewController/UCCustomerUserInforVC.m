//
//  UCCustomerUserInforVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerUserInforVC.h"
#import "UCAddUserCell.h"
#import "UCCustomerUserInforCell.h"

@interface UCCustomerUserInforVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;

@end

@implementation UCCustomerUserInforVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *array = @[ @[
                            @{@"title" : @"客户姓名" , @"placeholder" : @"请填写客户姓名(必填)" ,       @"value" : @"" },
                            @{@"title" : @"性别" ,    @"placeholder" : @"请选择客户性别" ,            @"value" : @"" },
                            @{@"title" : @"联系方式" , @"placeholder" : @"请填写客户联系方式(必填)" ,   @"value" : @"" },
                            @{@"title" : @"微信" ,    @"placeholder" : @"请填写客户微信号" ,          @"value" : @"" },
                            @{@"title" : @"地点" ,    @"placeholder" : @"请选择客户所在地区" ,         @"value" : @"" },
                            @{@"title" : @"商机" ,    @"placeholder" : @"请填写客户商机" ,            @"value" : @"" },
                            @{@"title" : @"客户级别" , @"placeholder" : @"请选择商机级别" ,           @"value" : @"" },
                            @{@"title" : @"备注" ,    @"placeholder" : @"请填写备注" ,               @"value" : @"" },
                        ],
                        @[
                            @{@"title" : @"创建时间" , @"placeholder" : @"" ,               @"value" : @"2019-12-22" },
                            @{@"title" : @"存在时间" ,    @"placeholder" : @"" ,            @"value" : @"20天" },
                            @{@"title" : @"跟踪次数" , @"placeholder" : @"" ,               @"value" : @"30次" },
                        ]
                    ];
    
    self.dataArray = [NSMutableArray array];
    for (NSArray *subArray  in array) {
        NSMutableArray *subMArray = [NSMutableArray array];
        for (NSDictionary *dic  in subArray) {
            [subMArray addObject:[[NSMutableDictionary alloc] initWithDictionary:dic]];
        }
        [self.dataArray addObject:subMArray];
       
    }
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    [tablView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"UITableViewHeaderFooterView"];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    tablView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
     return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *subArray = self.dataArray[section];
    return subArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *subArray = self.dataArray[indexPath.section];
    if (indexPath.section == 1) {
        UCCustomerUserInforCell *secondCell = [UCCustomerUserInforCell cellWithTableView:tableView];
         [secondCell configureWithViewModel:subArray[indexPath.row]];
        return secondCell;
    }else{
        UCAddUserCell *cell = [UCAddUserCell cellWithTableView:tableView];
        [cell configureWithViewModel:subArray[indexPath.row]];
          return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"UITableViewHeaderFooterView"];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return FLT_MIN;
    }
    return 11;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
