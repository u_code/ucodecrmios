//
//  UCAddUserOpportunityStageVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/2/17.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddUserOpportunityStageVC.h"
#import "UCOpportunityStageCell.h"

@interface UCAddUserOpportunityStageVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;
@property (nonatomic, strong) UIButton          *completeButton;

@end

@implementation UCAddUserOpportunityStageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.title = @"商机阶段";
    
    NSArray *array = @[
    @{@"stage" : @"阶段1" , @"percent" : @"赢单率达到10%" ,       @"isSelected" : @"0" },
    @{@"stage" : @"阶段2" , @"percent" : @"赢单率达到20%" ,       @"isSelected" : @"0" },
    @{@"stage" : @"阶段3" , @"percent" : @"赢单率达到50%" ,       @"isSelected" : @"0" },
    @{@"stage" : @"赢单" , @"percent" : @"赢单率100%" ,       @"isSelected" : @"0" },
    @{@"stage" : @"输单" , @"percent" : @"赢单率0%" ,       @"isSelected" : @"0" },
    
    ];
    
    self.dataArray = [NSMutableArray array];
    for (NSDictionary *dic  in array) {
        [self.dataArray addObject:[[NSMutableDictionary alloc] initWithDictionary:dic]];
    }
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    tablView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    
    UIView *bottomView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
       [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.left.bottom.right.equalTo(self.view);
           make.height.mas_equalTo(84);
           if (IS_IPhoneX) {
               
           }else{
               
           }
       }];
    
//    UIButton *completeButton = [UIButton buttonWithSuperView:bottomView title:@"完成" titleColor:[UIColor whiteColor] fontSize:16];
//        [completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(bottomView).offset(10);
//            make.right.equalTo(bottomView).offset(-30);
//            make.left.equalTo(bottomView).offset(30);
//            make.height.mas_equalTo(40);
//        }];
//        completeButton.layer.cornerRadius = 4;
//      completeButton.backgroundColor = [UIColor colorWithHexString:@"#53B1BF"];
//        completeButton.clipsToBounds = YES;
//        [completeButton addTarget:self action:@selector(addBusinessButtonActon:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

-(void)addBusinessButtonActon:(UIButton *)button{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCOpportunityStageCell *cell = [UCOpportunityStageCell cellWithTableView:tableView];
    [cell configureWithViewModel:_dataArray[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
