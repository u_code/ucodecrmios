//
//  UCSearchVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCSearchVC.h"
#import "UCSearchPlaceholderView.h"
#import "UCSearchBarView.h"
#import "UCSearchUserCell.h"
#import "UCSearchUserTableHeaderView.h"
#import "UCChatVC.h"

@interface UCSearchVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;
@property (nonatomic , strong) UCSearchPlaceholderView *searchPlaceholderView;

@end

@implementation UCSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UCSearchBarView *searchBar = [[UCSearchBarView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 16, 36)];
    self.navigationItem.titleView = searchBar;
    self.navigationItem.hidesBackButton = YES;
    
    UCSearchPlaceholderView *searchPlaceholderView = [UCSearchPlaceholderView viewWithSuperView:self.view];
    [searchPlaceholderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(20);
        make.left.width.equalTo(self.view);
        make.height.mas_equalTo(80);
    }];
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    [tablView registerClass:[UCSearchUserTableHeaderView class] forHeaderFooterViewReuseIdentifier:@"UCSearchUserTableHeaderView"];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCSearchUserCell *cell = [UCSearchUserCell cellWithTableView:tableView];
//    [cell configureWithViewModel:_dataArray[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 76;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UCChatVC *chatVc = [[UCChatVC alloc] init];
    [self.navigationController pushViewController:chatVc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
