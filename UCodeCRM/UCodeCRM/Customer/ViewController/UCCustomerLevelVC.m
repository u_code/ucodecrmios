//
//  UCCustomerLevelVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerLevelVC.h"
#import "UCOpportunityStageCell.h"

@interface UCCustomerLevelVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;
@property (nonatomic, strong) UIButton          *completeButton;

@end

@implementation UCCustomerLevelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.title = @"客户级别";
    
    NSArray *array = @[
    @{@"stage" : @"S级" , @"percent" : @"强需求" ,       @"isSelected" : @"0" },
    @{@"stage" : @"A级" , @"percent" : @"有需求" ,       @"isSelected" : @"0" },
    @{@"stage" : @"B级" , @"percent" : @"需求模糊" ,       @"isSelected" : @"0" },
    @{@"stage" : @"C级" , @"percent" : @"不放弃" ,       @"isSelected" : @"0" },
    @{@"stage" : @"D级" , @"percent" : @"放弃" ,       @"isSelected" : @"0" },
    
    ];
    
    self.dataArray = [NSMutableArray array];
    for (NSDictionary *dic  in array) {
        [self.dataArray addObject:[[NSMutableDictionary alloc] initWithDictionary:dic]];
    }
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    tablView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    
    UIView *bottomView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
       [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.left.bottom.right.equalTo(self.view);
           make.height.mas_equalTo(84);
           if (IS_IPhoneX) {
               
           }else{
               
           }
       }];
    
//    UIButton *completeButton = [UIButton buttonWithSuperView:bottomView title:@"完成" titleColor:[UIColor whiteColor] fontSize:16];
//        [completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(bottomView).offset(10);
//            make.right.equalTo(bottomView).offset(-30);
//            make.left.equalTo(bottomView).offset(30);
//            make.height.mas_equalTo(40);
//        }];
//        completeButton.layer.cornerRadius = 4;
//      completeButton.backgroundColor = [UIColor colorWithHexString:@"#53B1BF"];
//        completeButton.clipsToBounds = YES;
//        [completeButton addTarget:self action:@selector(addBusinessButtonActon:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

-(void)addBusinessButtonActon:(UIButton *)button{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCOpportunityStageCell *cell = [UCOpportunityStageCell cellWithTableView:tableView];
    [cell configureWithViewModel:_dataArray[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


@end
