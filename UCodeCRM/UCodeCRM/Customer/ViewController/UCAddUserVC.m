//
//  UCAddUserVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddUserVC.h"
#import "UCAddUserCell.h"
#import "UCAddUserBusinessVC.h"
#import "UCChooseSexVC.h"

@interface UCAddUserVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;
@property (nonatomic, strong) UIButton          *saveButton;



@end

@implementation UCAddUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.title = @"添加客户";
    
    UIButton *saveButton = [UIButton buttonWithSuperView:nil title:@"保存" titleColor:[UIColor whiteColor] fontSize:14];
    saveButton.frame = CGRectMake(0, 0, 48, 24);
    [saveButton addTarget:self action:@selector(saveUserInfor) forControlEvents:UIControlEventTouchUpInside];
    saveButton.backgroundColor = [[UIColor colorWithHexString:@"#23B3C1"] colorWithAlphaComponent:0.3];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    
    
    NSArray *array = @[
        @{@"title" : @"客户姓名" , @"placeholder" : @"请填写客户姓名(必填)" ,       @"value" : @"" , @"sel" : @""},
    @{@"title" : @"性别" ,    @"placeholder" : @"请选择客户性别" ,            @"value" : @"" , @"sel" : @"chooseSex:" },
    @{@"title" : @"联系方式" , @"placeholder" : @"请填写客户联系方式(必填)" ,   @"value" : @"" , @"sel" : @"" },
    @{@"title" : @"微信" ,    @"placeholder" : @"请填写客户微信号" ,          @"value" : @"" , @"sel" : @"" },
    @{@"title" : @"地点" ,    @"placeholder" : @"请选择客户所在地区" ,         @"value" : @"" , @"sel" : @"chooseAddress:" },
//    @{@"title" : @"商机" ,    @"placeholder" : @"请填写客户商机" ,            @"value" : @"" , @"sel" : @"" },
    @{@"title" : @"客户级别" , @"placeholder" : @"请选择商机级别" ,           @"value" : @"" , @"sel" : @"chooseLevel:" },
    @{@"title" : @"备注" ,    @"placeholder" : @"请填写备注" ,               @"value" : @"" , @"sel" : @"" },
    ];
    
    self.dataArray = [NSMutableArray array];
    for (NSDictionary *dic  in array) {
        [self.dataArray addObject:[[NSMutableDictionary alloc] initWithDictionary:dic]];
    }
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    tablView.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    
    UIView *bottomView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(84);
        if (IS_IPhoneX) {
            
        }else{
            
        }
    }];
    
    UIButton *addBusinessButton = [UIButton buttonWithSuperView:bottomView title:@"完成并添加商机" titleColor:[UIColor colorWithHexString:@"#53B1BF"] fontSize:16];
    [addBusinessButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView).offset(10);
        make.left.equalTo(bottomView).offset(30);
        make.right.equalTo(bottomView.mas_centerX).offset(-5);
        make.height.mas_equalTo(40);
    }];
    addBusinessButton.layer.cornerRadius = 4;
    addBusinessButton.layer.borderWidth = 1;
    addBusinessButton.layer.borderColor = [UIColor colorWithHexString:@"#53B1BF"].CGColor;
    addBusinessButton.clipsToBounds = YES;
    [addBusinessButton addTarget:self action:@selector(addBusinessButtonActon:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *completeButton = [UIButton buttonWithSuperView:bottomView title:@"完成" titleColor:[UIColor whiteColor] fontSize:16];
      [completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
          make.top.equalTo(bottomView).offset(10);
          make.right.equalTo(bottomView).offset(-30);
          make.left.equalTo(bottomView.mas_centerX).offset(5);
          make.height.mas_equalTo(40);
      }];
      completeButton.layer.cornerRadius = 4;
    completeButton.backgroundColor = [UIColor colorWithHexString:@"#53B1BF"];
      completeButton.clipsToBounds = YES;
      [completeButton addTarget:self action:@selector(addBusinessButtonActon:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)saveUserInfor{
    
}

-(void)addBusinessButtonActon:(UIButton *)button{
    UCAddUserBusinessVC *addUserBusinessVc = [[UCAddUserBusinessVC alloc] init];
    [self.navigationController pushViewController:addUserBusinessVc animated:YES];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCAddUserCell *cell = [UCAddUserCell cellWithTableView:tableView];
    NSDictionary *dic = _dataArray[indexPath.row];
    WEAK_REF(self);
    cell.textFieldShouldBeginEditing = ^BOOL (UITextField *textField){
        NSString *selString = dic[@"sel"];
        if ([weak_self respondsToSelector:NSSelectorFromString(selString)]) {
            
            [weak_self performSelector:NSSelectorFromString(selString) withObject:textField];
            return NO;
        }else{
            return YES;
        }
    };
    [cell configureWithViewModel:dic];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)chooseSex:(UITextField *)textField{
      UCChooseSexVC *chooseSex = [[UCChooseSexVC alloc] init];
         chooseSex.modalPresentationStyle = UIModalPresentationOverCurrentContext;
         chooseSex.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
         [self presentViewController:chooseSex animated:YES completion:^{
             
         }];
}

-(void)chooseAddress:(UITextField *)textField{
    
}

-(void)chooseLevel:(UITextField *)textField{
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
