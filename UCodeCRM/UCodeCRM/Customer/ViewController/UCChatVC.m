//
//  UCChatVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCChatVC.h"
#import "UCKeyboardView.h"
#import "UCChatCell.h"
#import "UCCustomerDetailVC.h"
#import "UCTrackingInformationVC.h"

@interface UCChatVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UCKeyboardView *keyBoardView;

@end

@implementation UCChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    self.title = @"刘洪林";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    
    
    UIButton *userDetailButton = [UIButton buttonWithSuperView:nil target:self action:@selector(userDetailButtonAction:) imageName:@"chat_user_detail"];
     UIBarButtonItem *userDetailItem = [[UIBarButtonItem alloc] initWithCustomView:userDetailButton];
    
    UIButton *moreThanButton = [UIButton buttonWithSuperView:nil target:self action:@selector(moreThanButtonAction:) imageName:@"chat_mor_than"];
    UIBarButtonItem *moreThanItem = [[UIBarButtonItem alloc] initWithCustomView:moreThanButton];
    
    self.navigationItem.rightBarButtonItems = @[moreThanItem,userDetailItem];
    
    
    
    UCKeyboardView *keyBoardView = [UCKeyboardView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
    [keyBoardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        if(IS_IPhoneX){
            make.height.mas_equalTo(83);
        }else{
           make.height.mas_equalTo(72);
        }
    }];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    tableView.backgroundColor  = [UIColor colorWithHexString:@"#F7F8F9"];
    
    tableView.separatorStyle = UITableViewScrollPositionNone;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 100;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(keyBoardView.mas_top);
    }];
    
    
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCChatCell *cell = [UCChatCell cellWithTableView:tableView];
    NSInteger messageType = arc4random()%4;
    
    cell.messageType = messageType;
    
  
    
    return cell;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    //收回键盘
    [[NSNotificationCenter defaultCenter] postNotificationName:@"keyboardHide" object:nil];
    //若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return YES;
}

-(void)userDetailButtonAction:(UIButton *)button{
    UCCustomerDetailVC *userDeailVc = [[UCCustomerDetailVC alloc] init];
    [self.navigationController pushViewController:userDeailVc animated:YES];
}

-(void)moreThanButtonAction:(UIButton *)button{
    UCTrackingInformationVC *trackingInformation = [[UCTrackingInformationVC alloc] init];
    [self.navigationController pushViewController:trackingInformation animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
