//
//  UCCustomerVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCCustomerVC.h"
#import "UCCustomerCell.h"
#import "UCCallVC.h"
#import "UCAddUserVC.h"
#import "UCSearchVC.h"
#import <MJRefresh/MJRefresh.h>

@interface UCCustomerVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation UCCustomerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *titleView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 60));
        make.top.left.equalTo(self.view);
    }];
    
    UILabel *titleLabel = [UILabel labelWithSuperView:titleView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont fontWithName:@"PingFangSC-Semibold" size:24] text:@"客户"];
   
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.equalTo(titleView).offset(20);
    }];
    
    UIButton *addButton = [UIButton buttonWithSuperView:titleView target:self action:@selector(addButtonAction:) imageName:@"homepage_add"];
       [addButton mas_makeConstraints:^(MASConstraintMaker *make) {
           make.size.mas_equalTo(CGSizeMake(40, 40));
           make.centerY.equalTo(titleLabel);
           make.right.equalTo(titleView.mas_right).offset(-11);
       }];
    
    UIButton *searchButton = [UIButton buttonWithSuperView:titleView target:self action:@selector(searchButtonAction:) imageName:@"homepage_search"];
    [searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
         make.centerY.equalTo(titleLabel);
         make.right.equalTo(addButton.mas_left).offset(1);
    }];
    
    
    
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
     [self.view addSubview:tablView];
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.view);
        make.top.equalTo(titleView.mas_bottom);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.left.bottom.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    tablView.backgroundColor = [UIColor redColor];
    self.view.backgroundColor = [UIColor purpleColor];
    tablView.mj_header = [MJRefreshHeader headerWithRefreshingBlock:^{
        [self getData];
    }];
    
    [tablView.mj_header beginRefreshing];
    
//    [self getData];
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
 
    return 103;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCCustomerCell *cell = [UCCustomerCell cellWithTableView:tableView];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 103;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UCCallVC *callVC = [[UCCallVC alloc] init];
       callVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
       callVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
       [self.tabBarController presentViewController:callVC animated:YES completion:^{
           
       }];
}


-(void)addButtonAction:(UIButton *)button{
    UCAddUserVC *addUser = [[UCAddUserVC alloc] init];
    addUser.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addUser animated:YES];
}

-(void)searchButtonAction:(UIButton *)button{
    UCSearchVC *search = [[UCSearchVC alloc] init];
   search.hidesBottomBarWhenPushed = YES;
   [self.navigationController pushViewController:search animated:YES];
}

-(void)getData{
    [[HttpEngine sharedHttpEngine] customerListWithKeywords:nil level:nil pageNum:1 pageSize:@"20" source:nil prole:@"0" userIds:nil  success:^(NSDictionary *dic) {
        [self.tableView.mj_header endRefreshing];
        if ([dic isSuccess]) {
            
        }else{
            [self.view showLoadingMeg:[dic errorMessage] time:kDefaultShowTime];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.view showLoadingMeg:kNetworkError time:kDefaultShowTime];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
