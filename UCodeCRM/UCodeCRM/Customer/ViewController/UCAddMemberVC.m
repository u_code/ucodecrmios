//
//  UCAddMemberVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCAddMemberVC.h"
#import "UCAddMemebrHeaderView.h"
#import "UCAddMemberCell.h"

@interface UCAddMemberVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray     *dataArray;

@end

@implementation UCAddMemberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.title = @"添加成员";
    
    UITableView *tablView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView = tablView;
    [self.view addSubview:tablView];
    [tablView registerClass:[UCAddMemebrHeaderView class] forHeaderFooterViewReuseIdentifier:@"UCAddMemebrHeaderView"];
    tablView.sectionHeaderHeight = 56;
    tablView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tablView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    tablView.delegate = self;
    tablView.dataSource = self;
    
    // Do any additional setup after loading the view.
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UCAddMemebrHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"UCAddMemebrHeaderView"];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UCAddMemberCell *cell = [UCAddMemberCell cellWithTableView:tableView];
//    [cell configureWithViewModel:_dataArray[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 76;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
