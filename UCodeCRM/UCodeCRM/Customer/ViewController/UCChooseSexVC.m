//
//  UCChooseSexVC.m
//  UCodeCRM
//
//  Created by dengyongjun on 2020/5/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCChooseSexVC.h"

@interface UCChooseSexVC ()

@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation UCChooseSexVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.5];
    
    UIButton *canCelButton = [UIButton buttonWithSuperView:self.view target:self action:@selector(cancelButtonAction) title:@"取消" titleColor:[UIColor colorWithHexString:@"#1A1616"]];
    canCelButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:17];
    [canCelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(59);
        make.bottom.left.right.equalTo(self.view).insets(UIEdgeInsetsMake(0, 20, 44, 20));
    }];
    canCelButton.backgroundColor = [UIColor whiteColor];
    canCelButton.layer.cornerRadius = 10;
    canCelButton.clipsToBounds = YES;
    
    UIView *contentView = [UIView viewWithSuperView:self.view backGroundColor:[UIColor whiteColor]];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 10;
    contentView.clipsToBounds = YES;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(70);
        make.bottom.left.right.equalTo(self.view).insets(UIEdgeInsetsMake(0, 20, 112, 20));
    }];
    
    NSArray *array = @[@{@"title" : @"男" , @"value" : @"1" },
                       @{ @"title": @"女" , @"value" : @"2" }];
    self.dataArray = array;
    UIView *lastView ;
    for (int i = 0; i < array.count; i++) {
        UIControl *control = [UIControl viewWithSuperView:contentView];
        control.tag = i;
        [control addTarget:self action:@selector(callPhoneAction:) forControlEvents:UIControlEventTouchUpInside];
        lastView = control;
        [control mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(44);
            make.left.right.equalTo(contentView);
            if (lastView) {
                make.bottom.equalTo(lastView.mas_top);
            }else{
                make.bottom.equalTo(contentView);
            }
        }];
        
        UILabel *nameLabel = [UILabel labelWithSuperView:control textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont fontWithName:@"PingFangSC-Medium" size:17] text:array[i]];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(control);
        }];
        
    }
    // Do any additional setup after loading the view.
}

-(void)callPhoneAction:(UIControl *)controller{
    [self cancelButtonAction];
}

-(void)cancelButtonAction{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if ([touch.view isEqual:self.view]) {
        [self cancelButtonAction];
    }
}

@end
