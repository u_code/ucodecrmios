//
//  UCChatInputOtherView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/10.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseView.h"
#import "UCChatInputOtherModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCChatInputOtherView : UCBaseView

@property (nonatomic, strong) NSArray  <UCChatInputOtherModel *> *dataArray;
@end

NS_ASSUME_NONNULL_END
