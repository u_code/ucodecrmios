//
//  UCSearchPlaceholderView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/2/19.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCSearchPlaceholderView.h"

@implementation UCSearchPlaceholderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        UILabel *titleLabel = [UILabel labelWithSuperView:self textColor:[UIColor colorWithHexString:@"#AAAAAA"] font:[UIFont systemFontOfSize:14] text:@"快速搜索指定内容"];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.centerX.equalTo(self);
        }];
        
        UIView *contentView = [UIView viewWithSuperView:self  ];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleLabel.mas_bottom).offset(15);
            make.left.width.bottom.equalTo(self);
            
        }];
        
        UILabel *nameLabel = [UILabel labelWithSuperView:contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"客户名"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(contentView);
            make.right.equalTo(contentView.mas_centerX).offset(-68);
        }];
        
        UILabel *orderLabel = [UILabel labelWithSuperView:contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"工单号"];
        [orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(contentView);
            make.centerX.equalTo(contentView);
        }];
        
        UILabel *phoneLabel = [UILabel labelWithSuperView:contentView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont systemFontOfSize:16] text:@"电话"];
        [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(contentView);
            make.left.equalTo(contentView.mas_centerX).offset(68);
        }];
        
    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
