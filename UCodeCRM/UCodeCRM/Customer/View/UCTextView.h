//
//  UCTextView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCTextView : UITextView

@property (nonatomic, strong) UIFont *textFont;

@property (nonatomic, assign) NSUInteger maxNumberOfLines;

@property (nonatomic, copy) void(^textHeightDidChange)(CGFloat height);

@end

NS_ASSUME_NONNULL_END
