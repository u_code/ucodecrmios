//
//  UCTrackingCollectionFooterView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCTrackingCollectionFooterView.h"
#import "UCBaseView.h"

@interface UCTrackingCollectionFooterView ()


@end

@implementation UCTrackingCollectionFooterView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
        UIButton *userNameSuperView = [UIButton buttonWithSuperView:self target:self action:@selector(userNameAction:) imageName:@""];
        [userNameSuperView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(10);
            make.left.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 56));
        }];
        userNameSuperView.backgroundColor = [UIColor whiteColor];
        
        UILabel *nameTitleLabel = [UILabel labelWithSuperView:userNameSuperView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"客户姓名"];
        [nameTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(userNameSuperView).offset(16);
            make.centerY.equalTo(userNameSuperView);
        }];
        
        UILabel *nameLabel = [UILabel labelWithSuperView:userNameSuperView textColor:[UIColor colorWithHexString:@"#666666"] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 16] text:@"刘洪林"];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(userNameSuperView).offset(-42);
            make.centerY.equalTo(userNameSuperView);
        }];
        
        UIImageView *arrowImage = [UIImageView imageViewWithSuperView:userNameSuperView imageName:@"arrow_right"];
        [arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(userNameSuperView).offset(-17);
            make.size.mas_equalTo(CGSizeMake(14, 14));
            make.centerY.equalTo(userNameSuperView);
        }];
        
        
        UIView *lineView = [UIView viewWithSuperView:userNameSuperView backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(userNameSuperView).offset(16);
            make.top.equalTo(userNameSuperView.mas_bottom);
            make.right.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        
        
        //聊天内容
        UIButton *chatSuperView = [UIButton buttonWithSuperView:self target:self action:@selector(chatContentAction:) imageName:@""];
        [chatSuperView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(userNameSuperView.mas_bottom).offset(0);
            make.left.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 56));
        }];
        chatSuperView.backgroundColor = [UIColor whiteColor];
        
        UILabel *chatTitleLabel = [UILabel labelWithSuperView:chatSuperView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"客户姓名"];
        [chatTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(chatSuperView).offset(16);
            make.centerY.equalTo(chatSuperView);
        }];
        
        
        UIImageView *chatArrowImage = [UIImageView imageViewWithSuperView:chatSuperView imageName:@"arrow_right"];
        [chatArrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(chatSuperView).offset(-17);
            make.size.mas_equalTo(CGSizeMake(14, 14));
            make.centerY.equalTo(chatSuperView);
        }];
        
        UIButton *moveCustomer = [UIButton buttonWithSuperView:self target:self action:@selector(moveCustomerAction:) title:@"将客户转至" titleColor:[UIColor colorWithHexString:@"#23B3C1"]];
        [moveCustomer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(chatSuperView.mas_bottom).offset(10);
            make.left.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 56));
        }];
        moveCustomer.backgroundColor = [UIColor whiteColor];
        
        
        
    }
    return self;
}

-(void)userNameAction:(UIButton *)button{
    
}

-(void)chatContentAction:(UIButton *)button{
    
}

-(void)moveCustomerAction:(UIButton *)button{
    
}

@end
