//
//  UCKeyboardView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCKeyboardView.h"
#import "UCTextView.h"
#import "UCChatInputOtherView.h"

@interface UCKeyboardView ()

@property (nonatomic, strong) UCTextView *textView;
@property (nonatomic, strong) UCChatInputOtherView *inputOtherView;
@property (nonatomic, strong) UITextField          *textField;
@property (nonatomic, assign) BOOL                 moreClick;
@property (nonatomic, assign) CGFloat              currentHeight;


@end

@implementation UCKeyboardView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.currentHeight = IS_IPhoneX ? 83 : 72;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        UCTextView *textView = [UCTextView viewWithSuperView:self backGroundColor:[UIColor colorWithHexString:@"#F6F6F6"]];
        [textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self).insets(UIEdgeInsetsMake(16, 16, 0, 68));
            if (IS_IPhoneX) {
                make.bottom.equalTo(self).offset(-27);
            }else{
                make.bottom.equalTo(self).offset(-16);
            }
        }];
        textView.layer.cornerRadius = 4;
        textView.maxNumberOfLines = 5;
        self.textView = textView;
        
        WEAK_REF(self);
        textView.textHeightDidChange = ^(CGFloat height) {
            
            CGFloat  minHeight = IS_IPhoneX ? 83 : 72;
            CGFloat shouldHeight =  height + 16 + (IS_IPhoneX ? 27 : 16);
            if (shouldHeight < minHeight) {
                shouldHeight = minHeight;
            }
            self.currentHeight = shouldHeight;
            NSLog(@"%F",shouldHeight);
            [UIView animateWithDuration:0.25 animations:^{
                [weak_self mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.bottom.right.equalTo(weak_self.superview);
                    make.height.mas_equalTo(shouldHeight);
                }];
            }];
            
            
        };
        
        UCChatInputOtherView *inputOtherView = [[UCChatInputOtherView alloc] initWithFrame:CGRectMake(0, IS_IPhoneX ? 83 : 72, SCREEN_WIDTH, IS_IPhoneX ? 175 : 141)];
        inputOtherView.hidden = YES;
        self.inputOtherView = inputOtherView;
        [self addSubview:inputOtherView];
        
        
        
        
        UIButton *otherButton = [UIButton buttonWithSuperView:self target:self action:@selector(otherButtonAction) imageName:@"chat_other"];
        [otherButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(IS_IPhoneX ? -33 : -22 );
            make.right.equalTo(self).offset( -22 );
            make.size.mas_equalTo(CGSizeMake(27, 27));
        }];
    }
    return self;
}

-(void)otherButtonAction{
    [self.textView resignFirstResponder];
    if (self.moreClick) {
        self.moreClick = NO;
        self.inputOtherView.hidden = YES;
        [UIView animateWithDuration:0.25 animations:^{
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.bottom.right.equalTo(self.superview);
                make.height.mas_equalTo(self.currentHeight);
            }];
        }];
        
    }else{
        
        self.moreClick = YES;
        self.inputOtherView.hidden = NO;
        [UIView animateWithDuration:0.25 animations:^{
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.superview);
                make.height.mas_equalTo(self.currentHeight);
                make.bottom.equalTo(self.superview).offset(-self.inputOtherView.frame.size.height);
            }];
        }];
    }
}

#pragma mark ====== 键盘将要出现 ======
- (void)keyboardWillShow:(NSNotification *)notification {
    
    if (self.moreClick) {
        self.moreClick = NO;
        self.inputOtherView.hidden = YES;
    }
}

#pragma mark ====== 键盘将要消失 ======
- (void)keyboardWillHide:(NSNotification *)notification {
    
    //如果是弹出了底部视图时
    if (self.moreClick) {
        return;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        [self mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self.superview);
            make.height.mas_equalTo(self.currentHeight);
        }];
    }];
    
    
}

@end
