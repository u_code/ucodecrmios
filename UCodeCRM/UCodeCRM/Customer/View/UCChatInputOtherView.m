//
//  UCChatInputOtherView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/10.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCChatInputOtherView.h"
#import "UCChatInputOtherCell.h"

@interface UCChatInputOtherView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation UCChatInputOtherView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *defaultData = @[
                                @{ @"title" : @"照片" , @"imageName" : @""},
                                @{ @"title" : @"拍摄" , @"imageName" : @""},
                                ];
        self.dataArray = [UCChatInputOtherModel modelListFromArray:defaultData];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(64, 88);
        layout.minimumInteritemSpacing = 24;
        layout.sectionInset = UIEdgeInsetsMake(14, 24, 14, 24);
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        [self addSubview:collectionView];
        [collectionView registerClass:[UCChatInputOtherCell class] forCellWithReuseIdentifier:@"UCChatInputOtherCell"];
        [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UCChatInputOtherCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UCChatInputOtherCell" forIndexPath:indexPath];
    [cell configureWithViewModel:self.dataArray[indexPath.row]];
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}


@end
