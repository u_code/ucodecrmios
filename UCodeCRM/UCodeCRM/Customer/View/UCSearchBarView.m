//
//  UCSearchBarView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCSearchBarView.h"


@implementation UCSearchBarView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [UIView viewWithSuperView:self backGroundColor:[UIColor whiteColor]];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(6);
            make.height.equalTo(self);
            make.width.mas_equalTo(SCREEN_WIDTH - 68);
            make.right.equalTo(self).offset(-62);
        }];
        view.layer.cornerRadius = 4;
        view.clipsToBounds = YES;
        
        UIButton *searchIcon = [UIButton buttonWithSuperView:view target:self action:@selector(searchIconAction:) imageName:@"homepage_search"];
        [searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view).offset(14);
            make.centerY.equalTo(view);
            make.size.mas_equalTo(CGSizeMake(19, 19));
        }];
        searchIcon.backgroundColor = [UIColor colorWithHexString:@"#F6F6F6"];
        self.searchIcon = searchIcon;
        
        UITextField *textField = [UITextField viewWithSuperView:view];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(searchIcon.mas_right).offset(11);
            make.height.equalTo(view);
            make.right.equalTo(view);
        }];
        self.textField = textField;
        textField.placeholder = @"搜索";
        
        UIButton *rightButton = [UIButton buttonWithSuperView:self target:self action:@selector(rightButtonAction:) title:@"取消" titleColor:[UIColor colorWithHexString:@"#1A1616"]];
        self.rightButton = rightButton;
        [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(view.mas_right);
            make.width.mas_equalTo(52);
            make.height.equalTo(self);
        }];
        
    }
    return self;
}

-(void)rightButtonAction:(UIButton *)button{
    
}

-(void)searchIconAction:(UIButton *)button{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
