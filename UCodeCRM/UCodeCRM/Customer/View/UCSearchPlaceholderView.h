//
//  UCSearchPlaceholderView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/2/19.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCSearchPlaceholderView : UCBaseView

@end

NS_ASSUME_NONNULL_END
