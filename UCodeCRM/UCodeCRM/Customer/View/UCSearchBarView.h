//
//  UCSearchBarView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCSearchBarView : UCBaseView

@property (nonatomic, strong) UIButton *searchIcon;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *rightButton;

@end

NS_ASSUME_NONNULL_END
