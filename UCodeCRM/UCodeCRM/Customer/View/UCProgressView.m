//
//  UCProgressView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/12.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCProgressView.h"

@interface UCProgressView ()

@property (nonatomic, assign) CGFloat   endAngle;
@property (nonatomic, strong) CAShapeLayer *solidLine;

@end

@implementation UCProgressView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        CAShapeLayer *solidLine =  [CAShapeLayer layer];
        solidLine.lineWidth = 3.0f ;
        solidLine.strokeColor = [UIColor colorWithHexString:@"#23B3C1"].CGColor;
        solidLine.fillColor = [UIColor clearColor].CGColor;
        self.solidLine = solidLine;
        self.endAngle = 0;
        
    }
    return self;
}


-(void)setProgress:(CGFloat)progress{
    
    CGMutablePathRef solidPath =  CGPathCreateMutable();
    CGPathAddRelativeArc(solidPath, nil, CGRectGetWidth(self.bounds)/2, CGRectGetWidth(self.bounds) /2, CGRectGetWidth(self.bounds)/2, -M_PI/2, progress * M_PI * 2);
    _solidLine.path = solidPath;
    CGPathRelease(solidPath);
    [self.layer addSublayer:_solidLine];
}

@end
