//
//  HttpEngine.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import "UCAdd.h"

typedef void(^AFResponseBlock) (NSDictionary *dic);
typedef void(^AFResponseErrorBlock) (NSError *error);

//正式服务器
//     /api-crm
//#define KHost @"https://gitee.com/u_code/ucode_commons/"
#define KHost @"http://123.57.129.177:8080/"
//http://123.57.129.177:8080

NS_ASSUME_NONNULL_BEGIN

@interface HttpEngine : NSObject
+ (HttpEngine *)sharedHttpEngine;

-(NSURLSessionDataTask *)logingWithUsername:(NSString *)username passWorld:(NSString *)passWorld success:(AFResponseBlock)success failure:(AFResponseErrorBlock)failure;

-(NSURLSessionDataTask *)customerPoolListWithKeywords:(NSString *)keywords level:(NSString *)level pageNum:(int  )pageNum pageSize:(NSString *)pageSize source:(NSString *)source success:(AFResponseBlock)success failure:(AFResponseErrorBlock)failure;

//非公海用户
-(NSURLSessionDataTask *)customerListWithKeywords:(NSString *)keywords level:(NSString *)level pageNum:(int  )pageNum pageSize:(NSString *)pageSize source:(NSString *)source prole:(NSString *)prole userIds:(NSArray *)userIds  success:(AFResponseBlock)success failure:(AFResponseErrorBlock)failure;

//创建
-(NSURLSessionDataTask *)customerCreateWithAddress:(NSString *)address areaId:(NSString *)areaId cityId:(NSString *)cityId contactsId:(NSString *)contactsId contactsName:(NSString *)contactsName  contactsSex:(NSString *)contactsSex industry:(NSString *)industry level:(NSString *)level mobile:(NSString *)mobile name:(NSString *)name nextTime:(NSString *)nextTime provinceId:(NSString *)provinceId remark:(NSString *)remark source:(NSString *)source telephone:(NSString *)telephone website:(NSString *)website success:(AFResponseBlock)success failure:(AFResponseErrorBlock)failure;
@end

@interface NSDictionary (UCAdd)

- (BOOL)isSuccess;
- (id )responseDic;
- (NSString *)errorMessage;

@end

NS_ASSUME_NONNULL_END
