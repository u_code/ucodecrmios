//
//  UCBaseNavigationController.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UCAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCBaseNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
