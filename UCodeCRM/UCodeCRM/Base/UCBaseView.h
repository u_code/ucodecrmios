//
//  UCBaseView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "Masonry.h"
#import "UCDefine.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <ReactiveObjC/RACReturnSignal.h>
#import "YSMacro.h"
#import "UIView+UCAdd.h"
#import "UIColor+YYAdd.h"
#import "UCBaseModel.h"
#import "UCAdd.h"
#import "UCUserModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface UCBaseView : UIView

@end

NS_ASSUME_NONNULL_END
