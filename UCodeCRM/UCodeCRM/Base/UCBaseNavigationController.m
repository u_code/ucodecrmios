//
//  UCBaseNavigationController.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseNavigationController.h"
#import "UIColor+YYAdd.h"
#import "UCBaseVC.h"
#import "UIImage+YYAdd.h"

@interface UCBaseNavigationController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@end

@implementation UCBaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.navigationBar setTintColor:kTextColor3];
    self.navigationBar.translucent = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor colorWithHexString:@"#1A1616"] , NSFontAttributeName :[UIFont boldSystemFontOfSize:20]}];
    CGRect rectOfStatusbar = [[UIApplication sharedApplication] statusBarFrame];
//    
    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#F6F6F6"] size:CGSizeMake(SCREEN_WIDTH, 100)] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
     self.interactivePopGestureRecognizer.delegate = self;
    self.delegate = self;
   
}
-(BOOL)shouldAutorotate
{
    
    if ([self.topViewController respondsToSelector:@selector(shouldAutorotate)]) {
        return [self.topViewController shouldAutorotate];
    }
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
