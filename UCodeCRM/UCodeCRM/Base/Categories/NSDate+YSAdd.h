//
//  NSDate+YSAdd.h
//  TemplateDemo
//
//  Created by 邓永军 on 16/1/16.
//  Copyright © 2016年 sure. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (YSAdd)

#pragma mark - Component Properties
///=============================================================================
/// @name Component Properties
///=============================================================================

@property (nonatomic, readonly) NSInteger year; ///< Year component
@property (nonatomic, readonly) NSInteger month; ///< Month component (1~12)
@property (nonatomic, readonly) NSInteger day; ///< Day component (1~31)
@property (nonatomic, readonly) NSInteger hour; ///< Hour component (0~23)
@property (nonatomic, readonly) NSInteger minute; ///< Minute component (0~59)
@property (nonatomic, readonly) NSInteger second; ///< Second component (0~59)
@property (nonatomic, readonly) NSInteger nanosecond; ///< Nanosecond component
@property (nonatomic, readonly) NSInteger weekday; ///< Weekday component (1~7, first day is based on user setting)
@property (nonatomic, readonly) NSInteger weekdayOrdinal; ///< WeekdayOrdinal component
@property (nonatomic, readonly) NSInteger weekOfMonth; ///< WeekOfMonth component (1~5)
@property (nonatomic, readonly) NSInteger weekOfYear; ///< WeekOfYear component (1~53)
@property (nonatomic, readonly) NSInteger yearForWeekOfYear; ///< YearForWeekOfYear component
@property (nonatomic, readonly) NSInteger quarter; ///< Quarter component
@property (nonatomic, readonly) BOOL isLeapMonth; ///< whether the month is leap month
@property (nonatomic, readonly) BOOL isLeapYear; ///< whether the year is leap year
@property (nonatomic, readonly) BOOL isToday; ///< whether date is today (based on current locale)
@property (nonatomic, readonly) BOOL isYesterday; ///< whether date is yesterday (based on current locale)

- (NSDate *)beginningOfDay;//返回日期当天凌晨的date
- (NSInteger)daysAgo;///距离现在的天数
- (NSInteger)daysAgoAgainstMidnight;///距离今天凌晨的天数
#pragma mark - Date modify

- (NSDate *)dateByAddingYears:(NSInteger)years;
- (NSDate *)dateByAddingMonths:(NSInteger)months;
- (NSDate *)dateByAddingWeeks:(NSInteger)weeks;
- (NSDate *)dateByAddingDays:(NSInteger)days;
- (NSDate *)dateByAddingHours:(NSInteger)hours;
- (NSDate *)dateByAddingMinutes:(NSInteger)minutes;
- (NSDate *)dateByAddingSeconds:(NSInteger)seconds;

#pragma mark - Date Format
///=============================================================================
/// @name Date Format
///=============================================================================

/**
 Returns a formatted string representing this date.
 see http://www.unicode.org/reports/tr35/tr35-31/tr35-dates.html#Date_Format_Patterns
 for format description.

 @param format   String representing the desired date format.
 e.g. @"yyyy-MM-dd HH:mm:ss"

 @return NSString representing the formatted date string.
 */
- (NSString *)stringWithFormat:(NSString *)format;

/**
 Returns a formatted string representing this date.
 see http://www.unicode.org/reports/tr35/tr35-31/tr35-dates.html#Date_Format_Patterns
 for format description.

 @param format    String representing the desired date format.
 e.g. @"yyyy-MM-dd HH:mm:ss"

 @param timeZone  Desired time zone.

 @param locale    Desired locale.

 @return NSString representing the formatted date string.
 */
- (NSString *)stringWithFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale;

/**
 Returns a string representing this date in ISO8601 format.
 e.g. "2010-07-09T16:13:30+12:00"

 @return NSString representing the formatted date string in ISO8601.
 */
- (NSString *)stringWithISOFormat;

/**
 Returns a date parsed from given string interpreted using the format.

 @param dateString The string to parse.
 @param format     The string's date format.

 @return A date representation of string interpreted using the format.
 If can not parse the string, returns nil.
 */
+ (NSDate *)dateWithString:(NSString *)dateString format:(NSString *)format;

/**
 Returns a date parsed from given string interpreted using the format.

 @param dateString The string to parse.
 @param format     The string's date format.
 @param timeZone   The time zone, can be nil.
 @param locale     The locale, can be nil.

 @return A date representation of string interpreted using the format.
 If can not parse the string, returns nil.
 */

+ (NSDate *)dateWithString:(NSString *)dateString format:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale;

/**
 Returns a date parsed from given string interpreted using the ISO8601 format.

 @param dateString The date string in ISO8601 format. e.g. "2010-07-09T16:13:30+12:00"

 @return A date representation of string interpreted using the format.
 If can not parse the string, returns nil.
 */
+ (NSDate *)dateWithISOFormatString:(NSString *)dateString;

+ (NSDate *)dateFromYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day;

@end
