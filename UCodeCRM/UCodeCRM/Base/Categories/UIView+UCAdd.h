//
//  UIView+UCAdd.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (UCAdd)

/**
 *    @brief  有text；
 */
+(UILabel *)labelWithSuperView:(UIView *)superView textColor:(UIColor *)textColor font:(UIFont *)font text:(NSString *)text;

/**
 *    @brief  没有text；
 */
+(UILabel *)labelWithSuperView:(UIView *)superView textColor:(UIColor *)textColor font:(UIFont *)font ;

@end

@interface UIImageView (UCAdd)

+(UIImageView *)imageViewWithSuperView:(UIView *)superView imageName:(NSString *)imageName;

@end

@interface UIButton (UCAdd)
/**
 *    @brief 所有的都有
 */
+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName title:(NSString *)title selectedTitle:(NSString *)selectedTitle titleColor:(UIColor *)titleColor selectedTitleColor:(UIColor *)selectedTitleColor backgroundColor:(UIColor *)backGroundColor;
/**
 *    @brief  没有选中状态 + backGroundColor + image；
 */
+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor;

/**
 *    @brief  没有选中状态 + image；
 */
+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor ;

/**
 *    @brief  没有选中状态 + backGroundColor ；
 */
+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action  title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor;

/**
 *    @brief  没有选中状态 ；
 */
+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action  title:(NSString *)title  titleColor:(UIColor *)titleColor ;

/**
 *    @brief  只有图片 ；
 */
+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action  imageName:(NSString *)imageName;


+(UIButton *)buttonWithSuperView:(UIView *)superView title:(NSString *)title  titleColor:(UIColor *)titleColor fontSize:(CGFloat )fontSize;

/**
 *    @brief 所有的都有
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView  imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName title:(NSString *)title selectedTitle:(NSString *)selectedTitle titleColor:(UIColor *)titleColor selectedTitleColor:(UIColor *)selectedTitleColor backgroundColor:(UIColor *)backGroundColor;
/**
 *    @brief  没有选中状态 + backGroundColor + image；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView  imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor;

/**
 *    @brief  没有选中状态 + image；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor ;

/**
 *    @brief  没有选中状态 + backGroundColor ；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView   title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor;

/**
 *    @brief  没有选中状态 ；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView  title:(NSString *)title  titleColor:(UIColor *)titleColor ;

/**
 *    @brief  只有图片 ；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView   imageName:(NSString *)imageName;

@end

@interface UIView (UCAdd)

+(instancetype )viewWithSuperView:(UIView *)superView backGroundColor:(UIColor *)backGroundColor;
+(instancetype )viewWithSuperView:(UIView *)superView ;
@end

@interface UIControl (UCAdd)

-(void)MaddTargeBlock:(void(^)(void))blocl;

@end
