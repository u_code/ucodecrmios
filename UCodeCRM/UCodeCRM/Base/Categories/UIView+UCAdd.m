//
//  UIView+UCAdd.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UIView+UCAdd.h"
#import <objc/runtime.h>

@implementation UILabel (UCAdd)

+(UILabel *)labelWithSuperView:(UIView *)superView textColor:(UIColor *)textColor font:(UIFont *)font text:(NSString *)text{
    UILabel *lable = [[UILabel alloc] init];
    lable.font = font;
    lable.textColor = textColor;
    [superView addSubview:lable];
    lable.text = text;
    return lable;
}

+(UILabel *)labelWithSuperView:(UIView *)superView textColor:(UIColor *)textColor font:(UIFont *)font{
   return  [self labelWithSuperView:superView textColor:textColor font:font text:nil];
}
@end


@implementation UIImageView (UCAdd)

+(UIImageView *)imageViewWithSuperView:(UIView *)superView imageName:(NSString *)imageName{
    UIImageView *imageView = [[UIImageView alloc] init];
    [superView addSubview:imageView];
    if (imageName && imageName.length > 0) {
         imageView.image = [UIImage imageNamed:imageName];
    }
   
    return imageView;
}

@end

@implementation UIButton (UCAdd)

+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName title:(NSString *)title selectedTitle:(NSString *)selectedTitle titleColor:(UIColor *)titleColor selectedTitleColor:(UIColor *)selectedTitleColor backgroundColor:(UIColor *)backGroundColor{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    if (imageName && imageName.length > 0) {
         [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
   
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    [button setTitle:selectedTitle forState:UIControlStateSelected];
    if (selectedImageName && selectedImageName.length > 0) {
         [button setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
    }
   
    [button setTitleColor:selectedTitleColor forState:UIControlStateSelected];
    button.backgroundColor = backGroundColor;
    return button;
}

+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    [button setTitleColor:titleColor forState:UIControlStateNormal];
     button.backgroundColor = backGroundColor;
    return button;

}

+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    return button;

}

+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action  title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.backgroundColor = backGroundColor;
    return button;

}

+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action  title:(NSString *)title  titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    return button;
}

+(UIButton *)buttonWithSuperView:(UIView *)superView target:(id)target action:(SEL)action  imageName:(NSString *)imageName{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    return button;
}

+(UIButton *)buttonWithSuperView:(UIView *)superView title:(NSString *)title  titleColor:(UIColor *)titleColor fontSize:(CGFloat )fontSize{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    return button;
}

/**
 *    @brief 所有的都有
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView  imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName title:(NSString *)title selectedTitle:(NSString *)selectedTitle titleColor:(UIColor *)titleColor selectedTitleColor:(UIColor *)selectedTitleColor backgroundColor:(UIColor *)backGroundColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button setTitle:title forState:UIControlStateNormal];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    [button setTitle:selectedTitle forState:UIControlStateSelected];
    if (selectedImageName && selectedImageName.length > 0 ) {
        [button setImage:[UIImage imageNamed:selectedImageName] forState:UIControlStateSelected];
    }
    
    [button setTitleColor:selectedTitleColor forState:UIControlStateSelected];
    button.backgroundColor = backGroundColor;
    return button;
}
/**
 *    @brief  没有选中状态 + backGroundColor + image；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView  imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button setTitle:title forState:UIControlStateNormal];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.backgroundColor = backGroundColor;
    return button;
}

/**
 *    @brief  没有选中状态 + image；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView imageName:(NSString *)imageName  title:(NSString *)title  titleColor:(UIColor *)titleColor {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button setTitle:title forState:UIControlStateNormal];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    return button;
}

/**
 *    @brief  没有选中状态 + backGroundColor ；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView   title:(NSString *)title  titleColor:(UIColor *)titleColor  backgroundColor:(UIColor *)backGroundColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.backgroundColor = backGroundColor;
    return button;
}

/**
 *    @brief  没有选中状态 ；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView  title:(NSString *)title  titleColor:(UIColor *)titleColor {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    return button;
}

/**
 *    @brief  只有图片 ；
 */
+(UIButton *)racbuttonWithSuperView:(UIView *)superView   imageName:(NSString *)imageName{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [superView addSubview:button];
    if (imageName && imageName.length > 0) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    return button;
}

@end

@implementation UIView (UCAdd)

+(instancetype )viewWithSuperView:(UIView *)superView backGroundColor:(UIColor *)backGroundColor{
    UIView *view = [[self alloc] initWithFrame:CGRectZero];
    [superView addSubview:view];
    view.backgroundColor = backGroundColor;
    return view;
}

+(instancetype )viewWithSuperView:(UIView *)superView{
    UIView *view = [[self alloc] initWithFrame:CGRectZero];
    [superView addSubview:view];
    return view;
}


@end

@implementation UIControl (UCAdd)

static NSString * const kXYTableViewPropertyInitFinish = @"kXYTableViewPropertyInitFinish";


- (void)setIsInitFinish:(void(^)(void))blocl {
    objc_setAssociatedObject(self, &kXYTableViewPropertyInitFinish, blocl, OBJC_ASSOCIATION_ASSIGN);
}


- (void(^)(void))isInitFinish {
    id obj = objc_getAssociatedObject(self, &kXYTableViewPropertyInitFinish);
//    void(^block)(void) = obj;
    return obj;
}

-(void)MaddTargeBlock:(void(^)(void))blocl{
    [self addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self setIsInitFinish:blocl];
}
//- (void)addTarget:(nullable id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvent{
////    objc_get
//}

-(void)buttonAction:(UIButton *)button{
    
    NSStringFromSelector(@selector(buttonAction:));
    NSString *s = @"buttonAction:";
    @selector(s);
    self.isInitFinish();
    
}
@end

