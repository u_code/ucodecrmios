//
//  UCTabBarController.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCTabBarController.h"
#import "UCBaseNavigationController.h"
#import "UCCustomerVC.h"

@interface UCTabBarController ()

@end

@implementation UCTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
     NSArray *dataArray = @[
                               @{
                                   @"title":@"客户",
                                   @"class":@"UCCustomerVC",
                                   @"image":@"tabbar_customer",
                                   @"selectedImage":@"tabbar_customer_selected"
                                   },
                               @{
                                   @"title":@"线索",
                                   @"class":@"UCClueVC",
                                   @"image":@"tabbar_headline",
                                   @"selectedImage":@"tabbar_customer"
                                   },
                               @{
                                   @"title":@"我的",
                                   @"class":@"UCMyVC",
                                   @"image":@"tabbar_my",
                                   @"selectedImage":@"tabbar_my_selected"
                                   }
                               ];

        
        
        
        NSMutableArray *viewControllerArray = [NSMutableArray array];

        for (NSDictionary *dic  in dataArray) {
            Class class = NSClassFromString(dic[@"class"]);
            UIViewController *viewController = [[class alloc] init];
            viewController.title = dic[@"title"];
            viewController.tabBarItem.image = [[UIImage imageNamed:dic[@"image"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            viewController.tabBarItem.selectedImage = [[UIImage imageNamed:dic[@"selectedImage"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//            [viewController.tabBarItem themeSetSelectedImage:dic[@"selectedImage"]];
    //        .selectedImage = [[UIImage imageNamed:dic[@"selectedImage"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
             selectTextAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#23B3C1"];
            [viewController.tabBarItem setTitleTextAttributes:selectTextAttrs forState:UIControlStateSelected];
//            [viewController.tabBarItem themeSetsetTitleColorforState:UIControlStateSelected SelectedImage:@"tabbarItemSelectedColor"];
            
            UCBaseNavigationController *navigationController = [[UCBaseNavigationController alloc] initWithRootViewController:viewController];
//            navigationController.topViewController
            [viewControllerArray addObject:navigationController];
        }
        self.viewControllers = viewControllerArray;
        self.tabBar.translucent = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
