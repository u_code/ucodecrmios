//
//  UCBaseModel.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import <YYModel/NSObject+YYModel.h>
#import "UCAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCBaseModel : NSObject

+ (instancetype)model;
+ (instancetype)modelWithDic:(NSDictionary *)dic;

- (instancetype)initWithDic:(NSDictionary *)dic;
- (void)setDataWithDic:(NSDictionary *)dic;

//通过数据创建
+ (NSMutableArray *)modelListFromArray:(NSArray *)arr;

//合并2个dic，可以传nil
+ (NSDictionary *)combineDic:(NSDictionary *)dic dic:(NSDictionary *)dic2;

@end

NS_ASSUME_NONNULL_END
