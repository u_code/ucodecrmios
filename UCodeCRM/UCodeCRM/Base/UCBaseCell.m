//
//  UCBaseCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/1/7.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseCell.h"

@implementation UCBaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithViewModel:(UCBaseModel *)ViewModel{
    
}

+ (CGFloat)heightForViewModel:(UCBaseModel *)model{
    return 44;
}
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    NSString *cellIdentifier = NSStringFromClass([self class]);
       UCBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
       if (!cell) {
           cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
       }
       return cell;
}


@end
