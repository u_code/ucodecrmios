//
//  DWGlobal.h
//  DWiPhone
//
//  Created by steven sun on 6/18/12.
//  Copyright 2012 Steven sun. All rights reserved.
//

#if __has_feature(objc_instancetype)

#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname) \
+ (instancetype)shared##classname \
{ \
    static dispatch_once_t once; \
    static id __singleton__; \
    dispatch_once( &once, ^{ __singleton__ = [[self alloc] init]; } ); \
    return __singleton__; \
}\

#else

#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname) \
+ (classname *)shared##classname \
{ \
static dispatch_once_t once; \
static classname __singleton__; \
dispatch_once( &once, ^{ __singleton__ = [[self alloc] init]; } ); \
return __singleton__; \
}\

#endif

