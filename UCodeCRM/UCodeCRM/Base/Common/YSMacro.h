//
//  YSMacro.h
//  TemplateDemo
//
//  Created by 邓永军 on 16/1/16.
//  Copyright © 2016年 sure. All rights reserved.
//

#ifndef YSMacro_h
#define YSMacro_h

// Screen Size
#define SCREEN_WIDTH			MIN(CGRectGetWidth([[UIScreen mainScreen] bounds]),CGRectGetHeight([[UIScreen mainScreen] bounds]))
#define SCREEN_HEIGHT           MAX(CGRectGetWidth([[UIScreen mainScreen] bounds]),CGRectGetHeight([[UIScreen mainScreen] bounds]))
#define SCREEN_SCALE			SCREEN_WIDTH/375.0
#define STATUSBAR_HEIGHT        [UIApplication sharedApplication].statusBarFrame.size.height


#define isIphone            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define isIpad              (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

//当前软件版本号
#define BundleShortVersionString    [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]


#define IS_IPHONE4 (([[UIScreen mainScreen] bounds].size.height == 480) ? YES : NO)

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height == 568) ? YES : NO)


#define IS_IPhone6 (667 == [[UIScreen mainScreen] bounds].size.height ? YES : NO)


#define IS_IPhone6plus (736 == [[UIScreen mainScreen] bounds].size.height ? YES : NO)

#define IS_IPhoneX (812 >= SCREEN_HEIGHT ? YES : NO)

// 设置ARBG
#define COLOR_WITH_ARGB(R,G,B,A)            [UIColor colorWithRed:R / 255.0 green:G / 255.0 blue:B / 255.0 alpha:A]
#define COLOR_WITH_RGB(R,G,B)               [UIColor colorWithRed:R / 255.0 green:G / 255.0 blue:B / 255.0 alpha:1]
#define COLOR_WITH_IMAGENAME(imageName)     [UIColor colorWithPatternImage:[UIImage imageNamed:imageName]]

#define WEAK_REF(obj) \
__weak typeof(obj) weak_##obj = obj; \

#endif /* YSMacro_h */
