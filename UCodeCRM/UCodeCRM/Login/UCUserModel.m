//
//  UCUserModel.m
//  UCodeCRM
//
//  Created by dengyongjun on 2020/5/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCUserModel.h"

#define UCODE_User_data @"UCODE_User_data"

@implementation UCUserModel

SYNTHESIZE_SINGLETON_FOR_CLASS(UCUserModel)

-(instancetype)init{
    self = [super init];
    if (self) {
         NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:UCODE_User_data];
        [self setDataWithDic:dic];
    }
    return self;
}

-(void)saveData:(NSDictionary *)dic{
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:UCODE_User_data];
}

-(void)clear{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionary] forKey:UCODE_User_data];
    unsigned int  ivarCount = 0 ;
    Ivar  *ivarList = class_copyIvarList(self.class, &ivarCount);
    for (int i = 0; i < ivarCount; i++) {
        Ivar var = ivarList[i];
        const char *varName = ivar_getName(var);
        NSString *name = [[NSString alloc] initWithUTF8String:varName];
        [self setValue:nil forKey:name];
//        NSLog(@"%@",name);
    }
    
//    unsigned int  civarCount = 0 ;
//    objc_property_t   *civarList = class_copyPropertyList(self.class, &civarCount);
//    for (int i = 0; i < civarCount; i++) {
//        objc_property_t var = civarList[i];
//        const char *varName =  property_getName(var);
//        NSString *name = [[NSString alloc] initWithUTF8String:varName];
//        [self setValue:nil forKey:name];
//        NSLog(@"%@",name);
//    }
    
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
@end
