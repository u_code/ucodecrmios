//
//  UCUserModel.h
//  UCodeCRM
//
//  Created by dengyongjun on 2020/5/14.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseModel.h"
#import "SynthesizeSingleton.h"
#import <objc/message.h>

NS_ASSUME_NONNULL_BEGIN

@interface UCUserModel : UCBaseModel

+ (UCUserModel *)sharedUCUserModel;

@property (nonatomic, copy) NSString *deptId;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *headImg;
@property (nonatomic, copy) NSString *job;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *num;
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, copy) NSString *realname;
@property (nonatomic, strong) NSMutableArray *roles;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *statuz;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *username;

-(void)saveData:(NSDictionary *)dic;
-(void)clear;

@end

NS_ASSUME_NONNULL_END
