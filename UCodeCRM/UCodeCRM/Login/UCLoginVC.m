//
//  UCLoginVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCLoginVC.h"
#import "UCModifyPasswordItemView.h"
#import "UCTabBarController.h"

@interface UCLoginVC ()

@property (nonatomic, strong) UCModifyPasswordItemView *phoneView;
@property (nonatomic, strong) UCModifyPasswordItemView *passwordView;



@end

@implementation UCLoginVC

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *titlaLabel = [UILabel labelWithSuperView:self.view textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont fontWithName:@"PingFangSC-Semibold" size: 24] text:@"手机号登录"];
    [titlaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(138);
        make.left.equalTo(self.view).offset(30);
    }];
    
    UCModifyPasswordItemView *phoneView = [[UCModifyPasswordItemView alloc] initWithSuperView:self.view title:@"手机号码" placeholder:@"请输入手机号"];
       [phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(titlaLabel.mas_bottom).offset(64);
           make.left.equalTo(self.view).offset(30);
           make.right.equalTo(self.view).offset(-30);
           make.height.mas_equalTo(57);
       }];
//    phoneView.textField.text = @"dengyj";
    phoneView.textField.text = @"17733325100";
    self.phoneView = phoneView;
    
    UCModifyPasswordItemView *passwordView = [[UCModifyPasswordItemView alloc] initWithSuperView:self.view title:@"确认密码" placeholder:@"请再次输入新密码"];
       [passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(phoneView.mas_bottom).offset(19);
           make.left.equalTo(self.view).offset(30);
           make.right.equalTo(self.view).offset(-30);
           make.height.mas_equalTo(57);
       }];
    self.passwordView = passwordView;
    passwordView.textField.text = @"12345678";
    
    UIButton *loginButton = [UIButton buttonWithSuperView:self.view target:self action:@selector(loginButtonAction:) title:@"登录" titleColor:[UIColor whiteColor]];
    [loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(30);
        make.right.equalTo(self.view).offset(-30);
        make.height.mas_equalTo(48);
        make.top.equalTo(passwordView.mas_bottom).offset(19);
    }];
    loginButton.backgroundColor = [UIColor colorWithHexString:@"#23B3C1"];
    loginButton.layer.cornerRadius = 4;
}

-(void)loginButtonAction:(UIButton *)button{
    
    [[HttpEngine sharedHttpEngine] logingWithUsername:self.phoneView.textField.text passWorld:self.passwordView.textField.text success:^(NSDictionary *dic) {
        if ([dic isSuccess]) {
            UCUserModel *user = [UCUserModel sharedUCUserModel];
            [user setDataWithDic:dic.responseDic];
            [user saveData:dic.responseDic];
        }else{
            [self.view showLoadingMeg:dic.errorMessage time:kDefaultShowTime];
        }
        
    } failure:^(NSError *error) {
        [self.view showLoadingMeg:kNetworkError time:kDefaultShowTime];
    }];
    UIWindow* window = nil;
          if (@available(iOS 13.0, *)) {
              for (UIWindowScene* windowScene in [UIApplication sharedApplication].connectedScenes){
                 if (windowScene.activationState == UISceneActivationStateForegroundActive){
                     for (UIWindow *kewWindows in windowScene.windows) {
                         if (kewWindows.isKeyWindow) {
                             window = kewWindows;
                             break;
                             
                         }
                     }
                 }
              }
          }else{
              #pragma clang diagnostic push
              #pragma clang diagnostic ignored "-Wdeprecated-declarations"
                  // 这部分使用到的过期api
               window = [UIApplication sharedApplication].keyWindow;
              #pragma clang diagnostic pop
          }
    
    window.rootViewController = [[UCTabBarController alloc] init]  ;
    
}
@end
