//
//  UCModifyPasswordVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCModifyPasswordVC.h"
#import "UCModifyPasswordItemView.h"

@interface UCModifyPasswordVC ()

@property (nonatomic, strong) UCModifyPasswordItemView *oldPasswordView;
@property (nonatomic, strong) UCModifyPasswordItemView *passwordView;
@property (nonatomic, strong) UCModifyPasswordItemView *confirmPasswordView;


@end

@implementation UCModifyPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.title = @"修改密码";
    UCModifyPasswordItemView *oldPasswordView = [[UCModifyPasswordItemView alloc] initWithSuperView:self.view title:@"原密码" placeholder:@"请输入原密码"];
    [oldPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(47);
        make.left.equalTo(self.view).offset(30);
        make.right.equalTo(self.view).offset(-30);
        make.height.mas_equalTo(57);
    }];
    self.oldPasswordView = oldPasswordView;
    
    UCModifyPasswordItemView *newPasswordView = [[UCModifyPasswordItemView alloc] initWithSuperView:self.view title:@"新密码" placeholder:@"请输入新密码"];
       [newPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(oldPasswordView.mas_bottom).offset(19);
           make.left.equalTo(self.view).offset(30);
           make.right.equalTo(self.view).offset(-30);
           make.height.mas_equalTo(57);
       }];
    self.passwordView = newPasswordView;
    
    UCModifyPasswordItemView *confirmPasswordView = [[UCModifyPasswordItemView alloc] initWithSuperView:self.view title:@"确认密码" placeholder:@"请再次输入新密码"];
       [confirmPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(newPasswordView.mas_bottom).offset(19);
           make.left.equalTo(self.view).offset(30);
           make.right.equalTo(self.view).offset(-30);
           make.height.mas_equalTo(57);
       }];
    self.confirmPasswordView = confirmPasswordView;
    
    UIButton *confirmButton = [UIButton buttonWithSuperView:self.view target:self action:@selector(confirmButtonAction:) title:@"确认" titleColor:[UIColor whiteColor]];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(30);
        make.right.equalTo(self.view).offset(-30);
        make.height.mas_equalTo(48);
        make.top.equalTo(confirmPasswordView.mas_bottom).offset(19);
    }];
    confirmButton.backgroundColor = [UIColor colorWithHexString:@"#23B3C1"];
    confirmButton.layer.cornerRadius = 4;
    
}

-(void)confirmButtonAction:(UIButton *)button{
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [self.view resignFirstResponder];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
