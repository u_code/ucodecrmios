//
//  UCSetVC.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCSetVC.h"
#import "UCModifyPasswordVC.h"
#import "UCLoginVC.h"
#import "AppDelegate.h"
#import "SceneDelegate.h"

@interface UCSetVC ()

@end

@implementation UCSetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F7F8F9"];
    self.title = @"设置";
    
    UIButton *modifyView = [UIButton buttonWithSuperView:self.view target:self action:@selector(modifyPassword) imageName:@""];
    [modifyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_offset(56);
    }];
    modifyView.backgroundColor = [UIColor whiteColor];
    
    UILabel *modifyLabel = [UILabel labelWithSuperView:modifyView textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont systemFontOfSize:16] text:@"修改密码"];
    [modifyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(modifyView).offset(16);
        make.centerY.equalTo(modifyView);
    }];
    
    UIImageView *arrowImage = [UIImageView imageViewWithSuperView:modifyView imageName:@"arrow_right"];
    [arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(modifyView);
        make.right.equalTo(modifyView).offset(-20);
    }];
    
    UIButton *loginOut = [UIButton buttonWithSuperView:self.view target:self action:@selector(loginOutAction:) title:@"退出登录" titleColor:[UIColor colorWithHexString:@"#1A1616"]];
    loginOut.titleLabel.font = [UIFont systemFontOfSize:16];
    [loginOut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(modifyView.mas_bottom).offset(17);
        make.left.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 56));
    }];
    loginOut.backgroundColor = [UIColor whiteColor];
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)modifyPassword{
    UCModifyPasswordVC *modifyPasswordVC = [[UCModifyPasswordVC alloc] init];
    [self.navigationController pushViewController:modifyPasswordVC animated:YES];
}

-(void)loginOutAction:(UIButton *)button{
    [[UCUserModel sharedUCUserModel] clear];
    UIWindow* window = nil;
          if (@available(iOS 13.0, *)) {
              for (UIWindowScene* windowScene in [UIApplication sharedApplication].connectedScenes){
                 if (windowScene.activationState == UISceneActivationStateForegroundActive){
                     for (UIWindow *kewWindows in windowScene.windows) {
                         if (kewWindows.isKeyWindow) {
                             window = kewWindows;
                             break;
                             
                         }
                     }
                 }
              }
          }else{
              #pragma clang diagnostic push
              #pragma clang diagnostic ignored "-Wdeprecated-declarations"
                  // 这部分使用到的过期api
               window = [UIApplication sharedApplication].keyWindow;
              #pragma clang diagnostic pop
          }
    
    window.rootViewController = [[UCLoginVC alloc] init]  ;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
