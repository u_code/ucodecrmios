//
//  UCMyCell.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCMyCell.h"

@interface UCMyCell ()

@property (nonatomic, strong) UIImageView *iconImage;
@property (nonatomic, strong) UILabel     *titleLabel;
//@property (nonatomic, strong) 

@end

@implementation UCMyCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *iconImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@"my_set"];
        [iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(16);
            make.centerY.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(24, 24));
        }];
        self.iconImage = iconImage;
        
        UILabel *titleLabel = [UILabel labelWithSuperView:self.contentView textColor:[UIColor colorWithHexString:@""] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 16] text:@"设置"];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(iconImage.mas_right).offset(12);
            make.centerY.equalTo(iconImage);
        }];
        self.titleLabel = titleLabel;
        
        UIImageView *arrowImage = [UIImageView imageViewWithSuperView:self.contentView imageName:@"arrow_right"];
        [arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(10, 10));
            make.right.mas_equalTo(self.contentView).offset(-16);
            make.centerY.equalTo(self.contentView);
        }];
        
        UIView *lineView = [UIView viewWithSuperView:self.contentView backGroundColor:[UIColor colorWithHexString:@"#EEEEEE"]];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self.contentView).insets(UIEdgeInsetsMake(0, 52, 0, 16));
            make.height.mas_equalTo(0.5);
        }];
        
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
