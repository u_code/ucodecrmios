//
//  UCMyHeader.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCMyHeaderView.h"

@interface UCMyHeaderView ()

@property (nonatomic, strong) UIButton *headerButton;
@property (nonatomic, strong) UILabel   *nameLabel;
@property (nonatomic, strong) UILabel   *descriptionLabel;


@end


@implementation UCMyHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIButton *headerButton = [UIButton buttonWithSuperView:self target:self action:@selector(headerButtonAction:) imageName:@""];
        [headerButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(16);
            make.top.equalTo(self).offset(74);
            make.size.mas_equalTo(CGSizeMake(52, 52));
        }];
        headerButton.backgroundColor = [UIColor colorWithHexString:@"#dddddd"];
        headerButton.layer.cornerRadius = 26;
        headerButton.clipsToBounds = YES;
        
        UILabel *nameLanel = [UILabel labelWithSuperView:self textColor:[UIColor colorWithHexString:@"#373737"] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 24] text:@"张三"];
        [nameLanel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(headerButton.mas_right).offset(24);
            make.top.equalTo(headerButton).offset(2);
        }];
        
        UILabel *descriptionLabel = [UILabel labelWithSuperView:self textColor:[UIColor colorWithHexString:@"#999999"] font:[UIFont systemFontOfSize:9] text:@"部门：用车部    职位：铁军"];
        [descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLanel);
            make.top.equalTo(nameLanel.mas_bottom).offset(9);
        }];
        
        UIImageView *arrowImage = [UIImageView imageViewWithSuperView:self imageName:@"arrow_right"];
        [arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(headerButton);
            make.right.equalTo(self).offset(-16);
            make.size.mas_equalTo(CGSizeMake(5, 10));
        }];
        
        UIControl *bgController = [UIControl viewWithSuperView:self];
        [bgController mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.topMargin.equalTo(headerButton);
            make.bottom.equalTo(headerButton);
        }];
        [bgController addTarget:self action:@selector(bgCointrolAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    
    return self;
}

-(void)bgCointrolAction:(UIControl *)controller{
    
}

-(void)headerButtonAction:(UIButton *)button{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
