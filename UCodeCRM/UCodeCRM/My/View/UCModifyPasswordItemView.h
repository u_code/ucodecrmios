//
//  UCModifyPasswordItemView.h
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UCModifyPasswordItemView : UCBaseView

@property (nonatomic, strong) UITextField *textField;

-(instancetype)initWithSuperView:(UIView *)superView title:(NSString *)title placeholder:(NSString *)placeholder;


@end

NS_ASSUME_NONNULL_END
