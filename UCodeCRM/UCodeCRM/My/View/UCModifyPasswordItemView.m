//
//  UCModifyPasswordItemView.m
//  UCodeCRM
//
//  Created by 邓永军 on 2020/3/9.
//  Copyright © 2020 Huajin. All rights reserved.
//

#import "UCModifyPasswordItemView.h"

@implementation UCModifyPasswordItemView

-(instancetype)initWithSuperView:(UIView *)superView title:(NSString *)title placeholder:(NSString *)placeholder{
    self = [super initWithFrame:CGRectZero];
    if (self) {
      
        NSLog(@"----------");
        [superView addSubview:self];
        UILabel *titleLabel = [UILabel labelWithSuperView:self textColor:[UIColor colorWithHexString:@"#1A1616"] font:[UIFont fontWithName:@"PingFangSC-Medium" size: 12] text:title];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self);
        }];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
        [self addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleLabel.mas_bottom).offset(6);
            make.left.equalTo(self);
            make.height.mas_equalTo(36);
            make.right.equalTo(self);
        }];
        textField.placeholder = placeholder;
        textField.font = [UIFont systemFontOfSize:16];
        self.textField = textField;
        
        UIView *lineView = [UIView viewWithSuperView:self backGroundColor:[UIColor colorWithHexString:@"#E8E8E8"]];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(self);
            make.height.mas_equalTo(0.5);
            make.left.right.bottom.equalTo(self);
        }];
        
    }
    return self;
}

@end
